import { UserSortBy } from './../enums/user-sort-by.enum';
import { DataPaginationDTO } from './../cve/models/data-pagination.dto';
import { DeleteUserDTO } from './models/delete-user.dto';
import {
  Controller,
  Post,
  Body,
  HttpCode,
  HttpStatus,
  UseGuards,
  UseInterceptors,
  UploadedFile,
  Put,
  Param,
  Delete,
  Get,
  Query
} from '@nestjs/common';
import { CreateUserDTO } from './models/create-user.dto';
import { UserDTO } from './models/user.dto';
import { UsersService } from './users.service';
import { AdminGuard } from '../common/guards/admin.guard';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { User } from '../common/decorators/user.decorator';
import { UpdateUserDTO } from './models/update-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { SortType } from '../enums/sort-type.enum';

@Controller('users')
@UseGuards(AuthGuardWithBlacklisting)
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('/:username')
  @HttpCode(HttpStatus.OK)
  public async getUser(@Param('username') username: string): Promise<UserDTO> {
    return await this.usersService.userByUsernameDTO(username);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  @UseGuards(AdminGuard)
  public async addNewUser(@Body() user: CreateUserDTO): Promise<UserDTO> {
    return await this.usersService.createUser(user);
  }

  @Put('/:username')
  @HttpCode(HttpStatus.CREATED)
  public async updateUser(
    @Param('username') username: string,
    @User() user: UserDTO,
    @Body() body: UpdateUserDTO
  ): Promise<UserDTO> {
    return await this.usersService.updateUser(username, user, body);
  }

  @Post('/:username/avatar')
  @HttpCode(HttpStatus.CREATED)
  @UseInterceptors(FileInterceptor('file'))
  public async updateUserAvatar(
    @Param('username') username: string,
    @User() user: UserDTO,
    @UploadedFile() file: any
  ): Promise<UserDTO> {
    const avatar: UpdateUserDTO = {
      avatarUrl: file.filename
    };
    return await this.usersService.updateUserAvatar(username, user, avatar);
  }

  @Put('/:username/password')
  @HttpCode(HttpStatus.CREATED)
  public async updateUserPassword(
    @Param('username') username: string,
    @User() user: UserDTO,
    @Body() body: UpdateUserDTO
  ): Promise<UserDTO> {
    return await this.usersService.updateUserPassword(username, user, body);
  }

  @Delete('/:username')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async deleteUser(
    @Param('username') username: string,
    @User() loggedUser: UserDTO
  ): Promise<DeleteUserDTO> {
    const deletedUser: UserDTO = await this.usersService.deleteUser(username, loggedUser);
    return { message: `User ${deletedUser.username} has been removed successfully!` };
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AdminGuard)
  public async allUsers(@Query() query): Promise<DataPaginationDTO<UserDTO[]>> {
    let { page, resultsPerPage, sortBy, sortType } = query;
    page = page || 1;
    sortBy = sortBy || UserSortBy.NAME;
    resultsPerPage = resultsPerPage || 10;
    sortType = sortType || SortType.ASC;

    return this.usersService.allUsers(page, resultsPerPage, sortBy, sortType);
  }
}
