import { UserSortBy } from './../enums/user-sort-by.enum';
import { DataPaginationDTO } from './../cve/models/data-pagination.dto';
import { UserDTO } from './../users/models/user.dto';
import { User } from './../database/entities/user.entity';
import {
  Injectable,
  BadRequestException,
  NotFoundException,
  UnauthorizedException,
  NotAcceptableException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDTO } from './models/create-user.dto';
import * as bcrypt from 'bcryptjs';
import { LoginUserDTO } from './models/login-user.dto';
import { UserRole } from '../enums/user-role.enum';
import { plainToClass } from 'class-transformer';
import { UpdateUserDTO } from './models/update-user.dto';
import { SortType } from '../enums/sort-type.enum';
import { EmailService } from '../utils/email.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly emailService: EmailService
  ) {}

  public async validateUserPassword(user: LoginUserDTO): Promise<boolean> {
    const userEntity: User = await this.usersRepository.findOne({
      username: user.username
    });

    return await bcrypt.compare(user.password, userEntity.password);
  }

  public async createUser(user: CreateUserDTO): Promise<UserDTO> {
    await this.checkIfUserExists(user);
    const userEntity: User = this.usersRepository.create(user);
    userEntity.password = await bcrypt.hash(user.password, 10);
    const createdUser: User = await this.usersRepository.save(userEntity);

    await this.emailService.sendMail(
      user.email,
      'Welcome to SVS!',
      `Hello, ${user.name}, we created for you a registration in our SVS platform.

    When you logged in to your account, you will be able to do the following:
    - Search by CVE-ID in CVE table, that use the NVD feeds
    - Search by product, vendor, CPE, and CVE-ID in our products table
    - Add or edit product which we use
    - Matching CPE and CVEs to product
    - Edit your account information
    - Notify our administrators of any vulnerabilities you have assigned.

    Best Regards,
    SVS`
    );

    return plainToClass(UserDTO, createdUser, {
      excludeExtraneousValues: true
    });
  }

  public async userByUsernameDTO(username: string): Promise<UserDTO> {
    const foundUser: User = await this.userByUsername(username);

    return plainToClass(UserDTO, foundUser, {
      excludeExtraneousValues: true
    });
  }

  public async updateUserAvatar(
    username: string,
    user: UserDTO,
    avatar: UpdateUserDTO
  ): Promise<UserDTO> {
    const foundUser: User = await this.userByUsername(username);

    if (this.checkIfUserHasRightsToEdit(user, username)) {
      const entityToUpdate: User = { ...foundUser, ...avatar };
      const savedUser: User = await this.usersRepository.save(entityToUpdate);

      return plainToClass(UserDTO, savedUser, {
        excludeExtraneousValues: true
      });
    }
  }

  public async updateUser(
    username: string,
    user: UserDTO,
    body: Partial<UpdateUserDTO>
  ): Promise<UserDTO> {
    const foundUser: User = await this.userByUsername(username);

    if (this.checkIfUserHasRightsToEdit(user, username)) {
      if (user.role === UserRole.BASIC) {
        delete body.role;
      }
      const userToUpdate: User = { ...foundUser, ...body };
      const savedUser: User = await this.usersRepository.save(userToUpdate);

      return plainToClass(UserDTO, savedUser, {
        excludeExtraneousValues: true
      });
    }
  }

  public async deleteUser(username: string, loggedUser: UserDTO): Promise<UserDTO> {
    const foundUser: User = await this.userByUsername(username);
    if (foundUser.id === +loggedUser.id) {
      throw new BadRequestException('You are not allowed to delete your own user account!');
    }
    const savedUser: User = await this.usersRepository.save({
      ...foundUser,
      isDeleted: true
    });

    return plainToClass(UserDTO, savedUser, {
      excludeExtraneousValues: true
    });
  }

  public async updateUserPassword(
    username: string,
    user: UserDTO,
    updatedUser: Partial<UpdateUserDTO>
  ): Promise<UserDTO> {
    const foundUser: User = await this.userByUsername(username);

    if (this.checkIfUserHasRightsToEdit(user, username)) {
      const arePasswordsEqual: Promise<boolean> = await bcrypt.compare(
        updatedUser.password,
        foundUser.password
      );

      if (arePasswordsEqual) {
        throw new NotAcceptableException(
          'The new password should not be the same as the old password.'
        );
      }

      updatedUser.password = await bcrypt.hash(updatedUser.password, 10);
      const userToUpdate: User = { ...foundUser, ...updatedUser };
      const savedUser: User = await this.usersRepository.save(userToUpdate);

      return plainToClass(UserDTO, savedUser, {
        excludeExtraneousValues: true
      });
    }
  }

  public async allUsers(
    page: number,
    resultsPerPage: number,
    sortBy: UserSortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<UserDTO[]>> {
    const pageIndex: number = resultsPerPage * (page - 1);
    const [users, totalResults]: [User[], number] = await this.usersRepository.findAndCount({
      where: { isDeleted: false },
      order: { [sortBy]: sortType },
      skip: pageIndex,
      take: resultsPerPage
    });

    const usersDTO: UserDTO[] = users.map((user) =>
      plainToClass(UserDTO, user, {
        excludeExtraneousValues: true
      })
    );

    const dataWithPagination: DataPaginationDTO<UserDTO[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: usersDTO
    };
    return dataWithPagination;
  }

  private async checkIfUserExists(user: CreateUserDTO): Promise<void> {
    const foundUserName: User = await this.usersRepository.findOne({
      username: user.username
    });

    if (foundUserName) {
      throw new BadRequestException('User with such username already exists!');
    }

    const foundUserEmail: User = await this.usersRepository.findOne({
      email: user.email
    });

    if (foundUserEmail) {
      throw new BadRequestException('User with such email already exists!');
    }
  }

  private async userByUsername(username: string): Promise<User> {
    const foundUser: User = await this.usersRepository.findOne({
      username,
      isDeleted: false
    });

    if (!foundUser) {
      throw new NotFoundException('No such user found');
    }

    return foundUser;
  }

  private checkIfUserHasRightsToEdit(user: UserDTO, username: string): boolean {
    if (username === user.username || user.role === UserRole.ADMIN) {
      return true;
    } else {
      throw new UnauthorizedException('The user is not allowed to perfrom this action!');
    }
  }
}
