import { IsString, IsNotEmpty, Length } from 'class-validator';

export class LoginUserDTO {
  @IsString()
  @IsNotEmpty()
  @Length(3, 16)
  public username: string;

  @IsString()
  @IsNotEmpty()
  @Length(5, 32)
  public password: string;
}
