import { Expose } from 'class-transformer/decorators';

export class DeleteUserDTO {
  @Expose()
  message: string;
}