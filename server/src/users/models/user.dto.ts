import { Expose } from 'class-transformer';
import { UserRole } from '../../enums/user-role.enum';

export class UserDTO {
  @Expose()
  public id: string;

  @Expose()
  public username: string;

  @Expose()
  public name: string;

  @Expose()
  public email: string;

  @Expose()
  public role: UserRole;

  @Expose()
  public avatarUrl: string;
}
