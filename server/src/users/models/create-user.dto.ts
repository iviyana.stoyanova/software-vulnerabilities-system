import { IsString, IsNotEmpty, Matches, IsEmail } from 'class-validator';
import { UserRole } from './../../enums/user-role.enum';

export class CreateUserDTO {
  @IsString()
  @IsNotEmpty()
  @Matches(/^(?=.{3,16}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/, {
    message: 'Username must be between 3 and 16 characters!'
  })
  public username: string;

  @IsString()
  @IsNotEmpty()
  @Matches(/^[a-zA-Z][a-zA-Z '-.,]{0,35}$|^$/, {
    message: 'Name must be between 3 and 35 characters!'
  })
  public name: string;

  @IsString()
  @IsNotEmpty()
  @Matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,32}$/, {
    message:
      'Password must be more than 5 characters containt at least one letter, one number and one special character!'
  })
  public password: string;

  @IsEmail()
  @IsNotEmpty()
  @Matches(
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    {
      message: 'Please, enter a valid email address!'
    }
  )
  public email: string;

  @IsNotEmpty()
  public role: UserRole;
}
