import { Controller, Get, Param, Query, HttpStatus, HttpCode } from '@nestjs/common';
import { CVEService } from './cve.service';
import { Observable } from 'rxjs';
import { CveDTO } from './models/cve.dto';
import { DataPaginationDTO } from './models/data-pagination.dto';

@Controller('cves')
export class CVEController {
  constructor(private readonly cveService: CVEService) {}

  @Get(':cveId')
  @HttpCode(HttpStatus.OK)
  cveById(@Param('cveId') cveId: string): Observable<DataPaginationDTO<CveDTO[]>> {
    return this.cveService.cveById(cveId);
  }

  @Get()
  @HttpCode(HttpStatus.OK)
  allCves(@Query() query): Observable<DataPaginationDTO<CveDTO[]>> {
    const { page, perPage } = query;
    return this.cveService.allCves(page, perPage);
  }
}
