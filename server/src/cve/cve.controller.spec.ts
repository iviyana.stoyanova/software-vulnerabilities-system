import { CVEController } from './cve.controller';
import { TestingModule, Test } from '@nestjs/testing';
import { CVEService } from './cve.service';
import { CveDTO } from './models/cve.dto';
import { DataPaginationDTO } from './models/data-pagination.dto';
import { Observable } from 'rxjs';

describe('CVEController', () => {
  let controller: CVEController;

  let cveService: any;

  beforeEach(async () => {
    cveService = {
      cveById() {
        /* empty */
      },
      allCves() {
        /* empty */
      }
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CVEController],
      providers: [
        {
          provide: CVEService,
          useValue: cveService
        }
      ]
    }).compile();

    controller = module.get<CVEController>(CVEController);
  });

  it('should be defined', () => {
    // Arrange, Act & Assert
    expect(controller).toBeDefined();
  });

  describe('cveById()', () => {
    it('should call cveService cveById() once with correct parameter', async () => {
      // Arrange
      const cveId = 'CVE-2018-18624';
      const spy = jest.spyOn(controller, 'cveById');

      // Act
      controller.cveById(cveId);

      // Assert
      expect(spy).toBeCalledTimes(1);
    });

    it('should call cveService cveById() and return correct data', async () => {
      // Arrange
      const cveId = 'CVE-2018-18624';
      const mockCVE: CveDTO[] = [
        {
          cveId,
          description:
            'Grafana 5.3.1 has XSS via a column style on the Dashboard > Table Panel screen.',
          lastModifiedDate: '2020-06-08T13:15Z',
          severity: 'MEDIUM'
        }
      ];
      const expectedResult: DataPaginationDTO<CveDTO[]> = {
        resultsPerPage: 1,
        page: 1,
        totalResults: 1,
        data: mockCVE
      };

      jest.spyOn(cveService, 'cveById').mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: Observable<DataPaginationDTO<CveDTO[]>> = await controller.cveById(cveId);

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });
  describe('allCves()', () => {
    it('should call cveService allCves() once with correct parameter', async () => {
      // Arrange
      const mockQuery = { page: 1, perPage: 10 };
      const spy = jest.spyOn(controller, 'allCves');

      // Act
      await controller.allCves(mockQuery);

      // Assert
      expect(spy).toBeCalledTimes(1);
    });

    it('should call cveService allCves() and return correct data', async () => {
      // Arrange
      const mockQuery = { page: 1, perPage: 10 };
      const mockCVE: CveDTO[] = [
        {
          cveId: 'CVE-2018-18624',
          description:
            'Grafana 5.3.1 has XSS via a column style on the Dashboard > Table Panel screen.',
          lastModifiedDate: '2020-06-08T13:15Z',
          severity: 'MEDIUM'
        }
      ];
      const expectedResult: DataPaginationDTO<CveDTO[]> = {
        resultsPerPage: 1,
        page: 1,
        totalResults: 1,
        data: mockCVE
      };

      jest.spyOn(cveService, 'allCves').mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: Observable<DataPaginationDTO<CveDTO[]>> = await controller.allCves(
        mockQuery
      );

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });
});
