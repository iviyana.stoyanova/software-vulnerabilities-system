import { CVEService } from './cve.service';
import { TestingModule, Test } from '@nestjs/testing';
import { HttpService } from '@nestjs/common';
import { of, Observable } from 'rxjs';
import { DataPaginationDTO } from './models/data-pagination.dto';
import { CveDTO } from './models/cve.dto';

describe('CVEService', () => {
  let service: CVEService;

  let httpService: any;

  beforeEach(async () => {
    httpService = {
      get() {
        /* empty */
      }
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [CVEService, { provide: HttpService, useValue: httpService }]
    }).compile();

    service = module.get<CVEService>(CVEService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('cveById()', () => {
    it('should call httpService get() once with the correct parameter', () => {
      // Arrange
      const baseUrl = 'https://services.nvd.nist.gov/rest/json/cve/1.0';
      const cveId = 'CVE-2018-18624';
      const url = `${baseUrl}/${cveId}`;
      // const mockCVE: CveDTO = new CveDTO();
      const mockCVE: CveDTO[] = [
        {
          cveId: 'CVE-2018-18624',
          description:
            'Grafana 5.3.1 has XSS via a column style on the Dashboard > Table Panel screen.',
          lastModifiedDate: '2020-06-08T13:15Z',
          severity: 'MEDIUM'
        }
      ];
      const expectedResult: DataPaginationDTO<CveDTO[]> = {
        resultsPerPage: 1,
        page: 1,
        totalResults: 1,
        data: mockCVE
      };

      const response = of(expectedResult);
      const spy = jest.spyOn(httpService, 'get').mockReturnValue(response);

      // Act
      service.cveById(cveId);

      // Assert
      expect(spy).toBeCalledWith(url);
      expect(spy).toBeCalledTimes(1);
    });

    it('should call cveService cveById() and return correct data', () => {
      // Arrange
      // const cveId = 'CVE-2018-18624';
      // const mockCVE: CveDTO = new CveDTO();
      // const expectedResult: DataPaginationDTO<CveDTO[]> = {
      //   resultsPerPage: 1,
      //   page: 1,
      //   totalResults: 1,
      //   data: mockCVE
      // };
      // const response = of(expectedResult);
      // const mockGet = jest.spyOn(httpService, 'get').mockReturnValue(response);
      // // Act
      // const actualResult: Observable<DataPaginationDTO<CveDTO[]>> = service.cveById(cveId);
      // // Assert
      // expect(response).toEqual(actualResult);
    });
  });

  describe('allCves()', () => {
    it('should call httpService get() once with the correct parameter', () => {
      // Arrange
      const cvesUrl = 'https://services.nvd.nist.gov/rest/json/cves/1.0';
      const page = '1';
      const perPage = '10';
      const index = +perPage * (+page - 1);
      const url = `${cvesUrl}?startIndex=${index}&resultsPerPage=${perPage}`;
      const mockCVE: CveDTO[] = [
        {
          cveId: 'CVE-2018-18624',
          description:
            'Grafana 5.3.1 has XSS via a column style on the Dashboard > Table Panel screen.',
          lastModifiedDate: '2020-06-08T13:15Z',
          severity: 'MEDIUM'
        }
      ];
      const expectedResult: DataPaginationDTO<CveDTO[]> = {
        resultsPerPage: 1,
        page: 1,
        totalResults: 1,
        data: mockCVE
      };

      const response = of(expectedResult);
      const spy = jest.spyOn(httpService, 'get').mockReturnValue(response);

      // Act
      service.allCves(page, perPage);

      // Assert
      expect(spy).toBeCalledWith(url);
      expect(spy).toBeCalledTimes(1);
    });
  });
});
