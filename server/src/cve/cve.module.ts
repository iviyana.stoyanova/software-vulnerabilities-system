import { Module, HttpModule } from '@nestjs/common';
import { CVEService } from './cve.service';
import { CVEController } from './cve.controller';

@Module({
  imports: [HttpModule],
  providers: [CVEService],
  exports: [CVEService],
  controllers: [CVEController]
})
export class CVEModule {}
