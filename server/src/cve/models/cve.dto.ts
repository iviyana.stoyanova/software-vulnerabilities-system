import { Expose, Transform } from 'class-transformer';
import { get } from 'lodash';

export class CveDTO {
  @Expose()
  @Transform((_value, obj) => get(obj, 'cve.CVE_data_meta.ID'))
  public cveId: string;

  @Expose()
  @Transform((_value, obj) => get(obj, 'cve.description.description_data[0].value'))
  public description: string;

  @Expose()
  @Transform((_value, obj) => get(obj, 'lastModifiedDate'))
  public lastModifiedDate: string;

  @Expose()
  @Transform((_value, obj) => get(obj, 'impact.baseMetricV2.severity'))
  public severity: string;
}
