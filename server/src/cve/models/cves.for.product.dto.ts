import { CveDTO } from './cve.dto';
import { Expose } from 'class-transformer';

export class CVEsForProductDTO {
  @Expose()
  public assignedCVEs: CveDTO[];

  @Expose()
  public unassignedCVEs: CveDTO[];
}