import { Expose } from 'class-transformer';

export class DataPaginationDTO<T> {
  @Expose()
  public resultsPerPage: number;

  @Expose()
  public page: number;

  @Expose()
  public totalResults: number;

  @Expose()
  public data: T;
}
