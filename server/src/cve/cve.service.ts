import { Injectable, HttpService, BadRequestException } from '@nestjs/common';
import { map, catchError } from 'rxjs/operators';
import { CveDTO } from './models/cve.dto';
import { Observable } from 'rxjs';
import { plainToClass } from 'class-transformer';
import { DataPaginationDTO } from './models/data-pagination.dto';

@Injectable()
export class CVEService {
  private readonly baseUrl: string = 'https://services.nvd.nist.gov/rest/json/cve/1.0';
  private readonly cvesUrl: string = 'https://services.nvd.nist.gov/rest/json/cves/1.0';

  constructor(private readonly httpService: HttpService) {}

  public cveById(cveId: string): Observable<DataPaginationDTO<CveDTO[]>> {
    const url = `${this.baseUrl}/${cveId}`;

    return this.httpService.get(url).pipe(
      map((res) => {
        const foundCve = res.data.result.CVE_Items[0];
        const cve = plainToClass(CveDTO, foundCve, {
          excludeExtraneousValues: true
        });

        const pagination = res.data;
        const cveDetails: DataPaginationDTO<CveDTO[]> = {
          resultsPerPage: pagination.resultsPerPage,
          page: 1,
          totalResults: pagination.totalResults,
          data: [cve]
        };

        return cveDetails;
      }),
      catchError((err) => {
        throw new BadRequestException(err.message);
      })
    );
  }

  public allCves(page: string, perPage: string): Observable<DataPaginationDTO<CveDTO[]>> {
    const index = +perPage * (+page - 1);
    const url = `${this.cvesUrl}?startIndex=${index}&resultsPerPage=${perPage}`;

    return this.httpService.get(url).pipe(
      map((res) => {
        const cvesList: CveDTO[] = res.data.result.CVE_Items.map((foundCve) => {
          return plainToClass(CveDTO, foundCve, {
            excludeExtraneousValues: true
          });
        });

        const pagination = res.data;
        const cvesDetails: DataPaginationDTO<CveDTO[]> = {
          resultsPerPage: pagination.resultsPerPage,
          page: +page,
          totalResults: pagination.totalResults,
          data: cvesList
        };

        return cvesDetails;
      }),
      catchError((err) => {
        throw new BadRequestException(err.message);
      })
    );
  }
}
