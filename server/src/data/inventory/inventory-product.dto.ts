import { IsString } from 'class-validator';

export class InventoryProductDTO {
  @IsString()
  public product: string;

  @IsString()
  public vendor: string;

  @IsString()
  public version: string;
}
