import * as bcrypt from 'bcryptjs';
import * as fs from 'fs';
import { Repository } from 'typeorm';
import { User } from '../../database/entities/user.entity';
import { UserRole } from '../../enums/user-role.enum';
import { Product } from '../../database/entities/product.entity';
import { InventoryProductDTO } from '../inventory/inventory-product.dto';
import { InjectRepository } from '@nestjs/typeorm';

export class SeedService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
    @InjectRepository(Product)
    private readonly productRepository: Repository<Product>
    ) {}

  public async seedDatabase(): Promise<void> {
    const createAdmin = () => {
      return new Promise(async (resolve) => {
        const admin: User = await this.userRepository.findOne({
          where: {
            username: 'admin'
          }
        });

        if (!admin) {
          const user = new User();
          user.id = 1;
          user.username = 'admin';
          user.name = 'Admin';
          user.email = 'svs_system_admin@yopmail.com';
          user.role = UserRole.ADMIN;
          const passwordHash = await bcrypt.hash('12345a$', 10);
          user.password = passwordHash;
          await this.userRepository.save(user);
          console.log('Admin was created successfully!');
        } else {
          console.log('Admin has been already created in the Database!');
        }
        resolve();
      });
    };

    const populateInventory = () => {
      return new Promise((resolve) => {
        fs.readFile('src/data/inventory/inventory.json', 'utf8', (err, value) => {
          if (!err) {
            const inventory: InventoryProductDTO[] = JSON.parse(value);
            let added = 0;
            inventory.forEach(async (item, index) => {
              const product = await this.productRepository.findOne({ where: { product: item.product } });
              if (!product) {
                added += 1;
                const newProduct = this.productRepository.create(item);
                await this.productRepository.save(newProduct);
              }

              if (index === inventory.length - 1) {
                console.log(
                  `${added} products from ${inventory.length} have been added in the Database!`
                );
                resolve();
              }
            });
          } else {
            console.error(err);
            resolve();
          }
        });
      });
    };

    console.time('It tooks');

    await createAdmin();
    await populateInventory();

    console.timeLog('It tooks', 'to seed the Database!');
  };
}
