import { SeedService } from './seed.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Module, HttpModule } from '@nestjs/common';
import { User } from '../../database/entities/user.entity';
import { Product } from '../../database/entities/product.entity';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([User, Product]),
  ],
  controllers: [],
  providers: [SeedService]
})
export class SeedModule {}
