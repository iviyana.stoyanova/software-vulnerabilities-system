import { CPEUpdater } from './../../cpe/cpe.updater.service';
import { NestFactory } from '@nestjs/core/nest-factory';
import { AppModule } from '../../app.module';

async function bootstrap() {
  NestFactory.createApplicationContext(AppModule)
    .then((appContext) => {
      const updater = appContext.get(CPEUpdater);
      updater
        .storeCPEs()
        .catch((error) => {
          console.log('CPE repository update failed!');
          throw error;
        })
        .finally(() => appContext.close());
    })
    .catch((error) => {
      throw error;
    });
}
bootstrap();
