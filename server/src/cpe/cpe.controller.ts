import { AdminGuard } from './../common/guards/admin.guard';
import { Controller, Get, HttpCode, HttpStatus, UseGuards, Put } from '@nestjs/common';
import { CPEUpdater } from './cpe.updater.service';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';

@Controller('cpe')
export class CPEController {
  constructor(private readonly cpeUpdater: CPEUpdater) {}

  @Put()
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public updateCPERepository(): void {
    this.cpeUpdater.storeCPEs();
  }

  @Get('/lastModifiedDate')
  @HttpCode(HttpStatus.OK)
  @UseGuards(AuthGuardWithBlacklisting, AdminGuard)
  public async getLastDateOfModification(): Promise<Date> {
    return await this.cpeUpdater.getLastDateOfModification();
  }
}
