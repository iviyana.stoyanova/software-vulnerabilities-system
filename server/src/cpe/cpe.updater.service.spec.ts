import { mock } from 'jest-mock-extended';
import { of } from 'rxjs';
import { HttpService } from '@nestjs/common';
import { CPE } from './../database/entities/cpe.entity';
import { TestingModule, Test } from '@nestjs/testing';
import { CPEUpdater } from './cpe.updater.service';
import { getRepositoryToken } from '@nestjs/typeorm';
import { SelectQueryBuilder } from 'typeorm';
describe('CPEUpdater', () => {
  let service: CPEUpdater;

  let cpesRepo: any;
  let httpService: any;

  beforeEach(async () => {
    cpesRepo = {
      findOne() {
        /* empty */
      },
      createQueryBuilder() {
        /* empty */
      },
      create() {
        /* empty */
      },
      save() {
        /* empty */
      }
    };

    httpService = {
      get() {
        /* empty */
      }
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CPEUpdater,
        { provide: getRepositoryToken(CPE), useValue: cpesRepo },
        { provide: HttpService, useValue: httpService }
      ]
    }).compile();

    service = module.get<CPEUpdater>(CPEUpdater);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('getCPEsStartingFrom()', () => {
    it('should call httpService get() once with the correct parameter', async () => {
      // Arrange
      const modStartDate: Date = new Date('2020-06-06T00:00:00.000Z');

      const responseData = { data: 'testData' };
      const response = of(responseData);
      const spy = jest
        .spyOn(httpService, 'get')
        .mockReturnValue(response);

      const correctURL =
        'https://services.nvd.nist.gov/rest/json/cpes/1.0?cpeMatchString=cpe:2.3:a:*&startIndex=1000&resultsPerPage=500&modStartDate=2020-06-06T00:00:00:000 Z&includeDeprecated=true';
      // Act
      await service.getCPEsStartingFrom(1000, 500, modStartDate);
      // Assert
      expect(spy).toBeCalledWith(correctURL);
      expect(spy).toBeCalledTimes(1);
    });
    it('should return the correct data', async () => {
      // Arrange
      const modStartDate: Date = new Date('2020-06-06T00:00:00.000Z');

      const responseData = { data: 'testData' };
      const response = of(responseData);
      jest
        .spyOn(httpService, 'get')
        .mockReturnValue(response);

      // Act
      const res = await service.getCPEsStartingFrom(1000, 500, modStartDate);
      // Assert
      expect(res).toEqual('testData');
    });
  });

  describe('getLastDateOfModification()', () => {
    it('should return the correct date when it exists', async () => {
      // Arrange
      const mockLastModifiedDate: Date = new Date('2012-04-06 00:27:00');
      const mockQueryBuilder = mock<SelectQueryBuilder<CPE>>();
      mockQueryBuilder.select.mockReturnThis();
      mockQueryBuilder.getRawOne.mockResolvedValue({ max: mockLastModifiedDate });
      jest
        .spyOn(cpesRepo, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      // Act
      const res: Date = await service.getLastDateOfModification();
      // Assert
      expect(res).toEqual(mockLastModifiedDate);
    });
    it('should return the correct date when it doesn\'t exist', async () => {
      // Arrange
      const mockQueryBuilder = mock<SelectQueryBuilder<CPE>>();
      mockQueryBuilder.select.mockReturnThis();
      mockQueryBuilder.getRawOne.mockResolvedValue({ max: null });
      jest
        .spyOn(cpesRepo, 'createQueryBuilder')
        .mockReturnValue(mockQueryBuilder);
      // Act
      const res: Date = await service.getLastDateOfModification();
      // Assert
      expect(res).toEqual(new Date(0));
    });
  });
  describe('storeCPEs()', () => {
    it('should call getCPEsStartingFrom 2 times with correct parameters when total results are 10 000', async () => {
      // Arrange
      jest.setTimeout(10000);
      const date: Date = new Date('2012-04-06 00:27:00');
      const mockCpeData = {
        totalResults: 10000,
        result: {
          cpes: []
        }
      };
      jest
        .spyOn(service, 'getLastDateOfModification')
        .mockResolvedValue(date);

      const getCPEsSpy = jest
        .spyOn(service, 'getCPEsStartingFrom')
        .mockReturnValue(mockCpeData);
      // Act
      await service.storeCPEs();
      // Assert
      expect(getCPEsSpy).toHaveBeenCalledTimes(2);
      expect(getCPEsSpy).toHaveBeenCalledWith(0, 5000, date);
      expect(getCPEsSpy).toHaveBeenCalledWith(5000, 5000, date);
    });
    it('should call getCPEsStartingFrom 2 times with correct parameters when total results are 10 000', async () => {
      // Arrange
      jest.setTimeout(10000);
      const date: Date = new Date('2012-04-06 00:27:00');
      const deprecatedArr: string[] = [
        'cpe:2.3:a:deprecatedBy1',
        'cpe:2.3:a:deprecatedBy2'
      ];
      const mockCpeData1 = {
        totalResults: 10000,
        result: {
          cpes: [{
            cpe23Uri: 'cpe:2.3:a:test1',
            lastModifiedDate: '2019-05-06 00:27:00',
            titles: [
              { title: 'Title1.1' },
              { title: 'Title1.2' }
            ],
            deprecatedBy: deprecatedArr
          }]
        }
      };

      const mockCpeData2 = {
        totalResults: 10000,
        result: {
          cpes: [{
            cpe23Uri: 'cpe:2.3:a:test2',
            lastModifiedDate: '2020-09-07 00:27:00',
            titles: [
              { title: 'Title2.1' },
              { title: 'Title2.2' }
            ],
            deprecatedBy: []
          }]
        }
      };

      const mockExistingCPE = new CPE();
      const mockEmptyCPE = new CPE();

      jest
        .spyOn(service, 'getLastDateOfModification')
        .mockResolvedValue(date);

      jest
        .spyOn(service, 'getCPEsStartingFrom')
        .mockResolvedValueOnce(mockCpeData1)
        .mockResolvedValueOnce(mockCpeData2);

      const findSpy = jest
        .spyOn(cpesRepo, 'findOne')
        .mockResolvedValueOnce(null)
        .mockResolvedValueOnce(mockExistingCPE);

      const createSpy = jest
        .spyOn(cpesRepo, 'create')
        .mockReturnValue(mockEmptyCPE);

      const saveSpy = jest
        .spyOn(cpesRepo, 'save')
        .mockResolvedValue(null);
      // Act
      await service.storeCPEs();
      // Assert
      expect(findSpy).toHaveBeenCalledTimes(2);
      expect(createSpy).toHaveBeenCalledTimes(1);
      expect(saveSpy).toHaveBeenCalledTimes(2);
      expect(saveSpy).toHaveBeenCalledWith([mockEmptyCPE]);
      expect(saveSpy).toHaveBeenCalledWith([mockExistingCPE]);
      expect(mockEmptyCPE.cpeUri).toEqual('cpe:2.3:a:test1');
      expect(mockEmptyCPE.lastModifiedDate).toEqual(new Date('2019-05-06 00:27:00'));
      expect(mockEmptyCPE.title).toEqual('Title1.1, Title1.2');
      expect(mockEmptyCPE.deprecatedBy).toEqual(JSON.stringify(deprecatedArr));
      expect(mockExistingCPE.cpeUri).toEqual('cpe:2.3:a:test2');
      expect(mockExistingCPE.lastModifiedDate).toEqual(new Date('2020-09-07 00:27:00'));
      expect(mockExistingCPE.title).toEqual('Title2.1, Title2.2');
      expect(mockExistingCPE.deprecatedBy).toEqual(null);
    });
  });
  describe('handleCron()', () => {
    it('should call storeCPEs() once', async () => {
        // Arrange
        const storeSpy = jest
          .spyOn(service, 'storeCPEs')
          .mockResolvedValue(null);
        // Act
        service.storeCPEs();
        // Assert
        expect(storeSpy).toHaveBeenCalledTimes(1);
    })
  })
});
