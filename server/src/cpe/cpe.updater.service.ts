import { Repository, SelectQueryBuilder } from 'typeorm';
import { Injectable, HttpService } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CPE } from '../database/entities/cpe.entity';
import { Cron } from '@nestjs/schedule';

@Injectable()
export class CPEUpdater {
  private readonly cpesSoftwareProductsUrl: string = 'https://services.nvd.nist.gov/rest/json/cpes/1.0?cpeMatchString=cpe:2.3:a:*';

  constructor(
    @InjectRepository(CPE)
    private readonly cpeRepository: Repository<CPE>,
    private readonly httpService: HttpService
    ) {}

  public async getCPEsStartingFrom(index: number, maxResultsPerPage: number, modStartDate: Date) {
    const modStartDateString: string = modStartDate.toISOString().replace('.', ':').replace('Z', ' Z');
    console.log(`Requesting ${this.cpesSoftwareProductsUrl}&startIndex=${index}&resultsPerPage=${maxResultsPerPage}&modStartDate=${modStartDateString}&includeDeprecated=true`);
    const res = await this.httpService
    .get(`${this.cpesSoftwareProductsUrl}&startIndex=${index}&resultsPerPage=${maxResultsPerPage}&modStartDate=${modStartDateString}&includeDeprecated=true`)
    .toPromise()
    .catch(async err => {
      console.log(err.message);
      console.log('Request to the API failed. Retrying after one minute.');
      await new Promise(resolve => setTimeout(resolve, 60000));
      return await this.getCPEsStartingFrom(index, maxResultsPerPage, modStartDate);
    });
    if (res.data) {
      return res.data;
    } else {
      console.log('API did not return the requested data. Retrying after one minute.');
      await new Promise(resolve => setTimeout(resolve, 60000));
      return await this.getCPEsStartingFrom(index, maxResultsPerPage, modStartDate);
    }
  }

  private async getCPEByUri(cpeUri: string): Promise<CPE> {
    const foundCPE: CPE = await this.cpeRepository.findOne({
      cpeUri
    });

    return foundCPE;
  }

  public async getLastDateOfModification(): Promise<Date> {
    const query: SelectQueryBuilder<CPE> = this.cpeRepository.createQueryBuilder('cpe');
    const lastModifiedDate = await (query.select('MAX(cpe.lastModifiedDate)', 'max'))
      .getRawOne();
    return lastModifiedDate.max ? lastModifiedDate.max : new Date(0);
  }

  public async storeCPEs(): Promise<void> {
    const maxResultsPerPage = 5000;
    let startIndex = 0;
    let totalResults: number;
    const dateOfLastUpdate: Date = await this.getLastDateOfModification();
    console.log(dateOfLastUpdate);
    do {
      const data = await this.getCPEsStartingFrom(startIndex, maxResultsPerPage, dateOfLastUpdate);
      console.log('Data retrieved');
      const cpeList = data.result.cpes;
      totalResults = data.totalResults;
      console.log(`Total results: ${totalResults}`);
      const entitiesToSave: CPE[] = await Promise.all(cpeList.map(async (cpe) => {
        let cpeEntity: CPE = await this.getCPEByUri(cpe.cpe23Uri);
        if (!cpeEntity) {
          cpeEntity = this.cpeRepository.create();
        }
        cpeEntity.cpeUri = cpe.cpe23Uri;
        cpeEntity.lastModifiedDate = new Date(cpe.lastModifiedDate);
        cpeEntity.title = cpe.titles.map((titleObject) => titleObject.title).join(', ');
        cpeEntity.deprecatedBy = cpe.deprecatedBy.length > 0 ? JSON.stringify(cpe.deprecatedBy) : null;
        return cpeEntity;
      }));
      await this.cpeRepository.save(entitiesToSave);
      console.log(`Saved entries at start index ${startIndex}`);
      startIndex += maxResultsPerPage;
      await new Promise(resolve => setTimeout(resolve, 2500));
    } while (startIndex < totalResults - 1);

    console.log('Database updated.');
    return null;
  }

  @Cron('0 0 0 * * *')
  handleCron() {
    this.storeCPEs();
  }
}