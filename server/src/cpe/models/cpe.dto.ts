import { Expose } from 'class-transformer';

export class CpeDTO {
  @Expose()
  public id: number;

  @Expose()
  public title: string;

  @Expose()
  public lastModifiedDate: Date;

  @Expose()
  public cpeUri: string;
}
