import { CPEUpdater } from './cpe.updater.service';
import { TestingModule, Test } from '@nestjs/testing';
import { CPEController } from './cpe.controller';
describe('CPEController', () => {
  let controller: CPEController;

  let cpeUpdaterService: any;

  beforeEach(async () => {
    cpeUpdaterService = {
      getCPEsStartingFrom() {
        /* empty */
      },
      getLastDateOfModification() {
        /* empty */
      },
      storeCPEs() {
        /* empty */
      }
    };
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CPEController],
      providers: [
        {
          provide: CPEUpdater,
          useValue: cpeUpdaterService
        }
      ]
    }).compile();

    controller = module.get<CPEController>(CPEController)
  });

  it('should be defined', () => {
    // Arrange, Act & Assert
    expect(controller).toBeDefined();
  });

  describe('updateCPERepository()', () => {
    it('should call cpeUpdater storeCPEs() once', async () => {
      // Arrange
      const spy = jest
        .spyOn(controller, 'updateCPERepository');
      // Act
      controller.updateCPERepository();
      // Assert
      expect(spy).toBeCalledTimes(1);
    });
  });
  describe('getLastDateOfModification()', () => {
    it('should call cpeUpdater getLastDateOfModification() once', async () => {
      // Arrange
      const spy = jest
        .spyOn(controller, 'getLastDateOfModification');
      // Act
      await controller.getLastDateOfModification();
      // Assert
      expect(spy).toBeCalledTimes(1);
    });
    it('should return the correct date', async () => {
      // Arrange
      const date: Date = new Date();
      jest
        .spyOn(controller, 'getLastDateOfModification')
        .mockResolvedValue(date);
      // Act
      const res = await controller.getLastDateOfModification();
      // Assert
      expect(res).toEqual(date);
    });
  });
});
