import { TypeOrmModule } from '@nestjs/typeorm';
import { CPEUpdater } from './cpe.updater.service';
import { Module, HttpModule } from '@nestjs/common';
import { CPEController } from './cpe.controller';
import { CPE } from '../database/entities/cpe.entity';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([CPE]),
  ],
  controllers: [CPEController],
  providers: [CPEUpdater]
})
export class CPEModule {}
