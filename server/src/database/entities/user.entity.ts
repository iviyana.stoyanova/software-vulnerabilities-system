import { Entity, PrimaryGeneratedColumn, Column, OneToMany } from 'typeorm';
import { UserRole } from '../../enums/user-role.enum';
import { Product } from './product.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false, unique: true, length: 30 })
  public username: string;

  @Column({ nullable: false, length: 35 })
  public name: string;

  @Column({ nullable: false, unique: true })
  public email: string;

  @Column({ nullable: false })
  public role: UserRole;

  @Column({ nullable: false })
  public password: string;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

  @Column({ nullable: true })
  public avatarUrl: string;

  @OneToMany(
    (type) => Product,
    (product) => product.user
  )
  public product: Promise<Product>;
}
