import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { Product } from './product.entity';

@Entity('cve_assignment')
export class CVEAssignment {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false, unique: true })
  public cveId: string;

  @ManyToOne(
    (type) => Product,
    (product) => product.cves
  )
  public product: Promise<Product>;
}
