import { Entity, PrimaryGeneratedColumn, Column, Index, OneToOne } from 'typeorm';
import { Product } from './product.entity';

@Entity('cpe')
export class CPE {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false, type: 'varchar', length: 600 })
  @Index()
  public title: string;

  @Column({ nullable: false })
  public lastModifiedDate: Date;

  @Column({ nullable: false })
  @Index({ unique: true })
  public cpeUri: string;

  @Column({ nullable: true, type: 'text' })
  public deprecatedBy: string;

  @OneToOne(
    (type) => Product,
    (product) => product.cpe,
    { lazy: true }
  )
  public product: Promise<Product>;
}
