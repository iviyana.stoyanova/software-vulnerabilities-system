import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  OneToMany,
  ManyToOne,
  OneToOne,
  CreateDateColumn,
  JoinColumn
} from 'typeorm';
import { CVEAssignment } from './cve.assignment.entity';
import { User } from './user.entity';
import { CPE } from './cpe.entity';

@Entity('products')
export class Product {
  @PrimaryGeneratedColumn('increment')
  public id: number;

  @Column({ nullable: false })
  public product: string;

  @Column({ nullable: false })
  public vendor: string;

  @Column({ nullable: false })
  public version: string;

  @Column({ nullable: false, default: false })
  public hasCves: boolean;

  @Column({ nullable: false, default: false })
  public hasCpe: boolean;

  @CreateDateColumn()
  public addedOn: string;

  @OneToMany(
    (type) => CVEAssignment,
    (cve) => cve.product,
    { lazy: true }
  )
  public cves: Promise<CVEAssignment[]>;

  @ManyToOne(
    (type) => User,
    (user) => user.product,
    { lazy: true }
  )
  public user: Promise<User>;

  @OneToOne(
    (type) => CPE,
    (cpe) => cpe.product,
    { lazy: true, cascade: true }
  )
  @JoinColumn()
  public cpe: Promise<CPE>;

  @Column({ nullable: false, default: false })
  public isDeleted: boolean;

}
