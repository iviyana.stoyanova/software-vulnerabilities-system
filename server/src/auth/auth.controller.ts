import { Controller, Post, Body, UseGuards, Delete } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDTO } from '../users/models/login-user.dto';
import { Token } from '../common/decorators/token.decorator';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';

@Controller('session')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
  ) {}

  @Post('')
  async login(@Body() user: LoginUserDTO): Promise<{ token: string }> {
    return await this.authService.login(user);
  }

  @Delete('')
  @UseGuards(AuthGuardWithBlacklisting)
  public async logout(@Token() token: string) {
    this.authService.blacklistToken(token);
    return {
      msg: 'Successful logout!',
    };
  }
}
