import { Injectable, NotFoundException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDTO } from '../users/models/login-user.dto';
import { UsersService } from '../users/users.service';
import { UserDTO } from '../users/models/user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from '../database/entities/user.entity';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';

@Injectable()
export class AuthService {
  private readonly blacklist: string[] = [];

  constructor(
    @InjectRepository(User) private readonly usersRepository: Repository<User>,
    private readonly userService: UsersService,
    private readonly jwtService: JwtService
  ) {}

  public async login(user: LoginUserDTO): Promise<any> {
    const foundUser: User = await this.usersRepository.findOne({
      where: { username: user.username, isDeleted: false }
    });

    if (!foundUser) {
      throw new NotFoundException('User with such username does not exist!');
    }

    if (!(await this.userService.validateUserPassword(user))) {
      throw new NotFoundException('Invalid password!');
    }

    const showUserPayload: UserDTO = plainToClass(UserDTO, foundUser, {
      excludeExtraneousValues: true
    });

    const payload: UserDTO = { ...showUserPayload };

    return {
      token: await this.jwtService.signAsync(payload)
    };
  }

  public blacklistToken(token: string): void {
    this.blacklist.push(token);
  }

  public isTokenBlacklisted(token: string): boolean {
    return this.blacklist.includes(token);
  }
}
