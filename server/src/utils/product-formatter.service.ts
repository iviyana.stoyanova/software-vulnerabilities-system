import { Injectable } from '@nestjs/common';
import { Product } from '../database/entities/product.entity';
import { ProductDTO } from '../products/models/product.dto';
import { plainToClass } from 'class-transformer';
import { CPE } from '../database/entities/cpe.entity';

@Injectable()
export class ProductFormatterService {
  public async productUsernameFormatter(product: Product): Promise<ProductDTO> {
    let checkUsername: string = await (await product.user).username;
    if (!checkUsername) {
      checkUsername = 'SVS';
    }

    return plainToClass(
      ProductDTO,
      { ...product, username: checkUsername },
      {
        excludeExtraneousValues: true
      }
    );
  }

  public async productDetailsFormatter(product: Product): Promise<ProductDTO> {
    const cpe: CPE = await product.cpe;
    const user = await product.user;

    // cves does not contain cves, it contains empty objects
    // need refactoring

    return plainToClass(
      ProductDTO,
      {
        ...product,
        cpe,
        username: user ? user.username : 'SVS'
      },
      {
        excludeExtraneousValues: true
      }
    );
  }
}
