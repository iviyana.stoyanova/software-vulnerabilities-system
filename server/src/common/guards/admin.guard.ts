import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { UserRole } from '../../enums/user-role.enum';
import { UserDTO } from '../../users/models/user.dto';

@Injectable()
export class AdminGuard implements CanActivate {
  public canActivate(context: ExecutionContext): boolean {
    const request = context.switchToHttp().getRequest();
    const user: UserDTO = request.user;
    return user && user.role === UserRole.ADMIN;
  }
}
