import { SeedModule } from './data/seed/seed.module';
import { CPEModule } from './cpe/cpe.module';
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { CoreModule } from './common/core.module';
import { CVEModule } from './cve/cve.module';
import { ProductsModule } from './products/products.module';
import { MailerModule } from '@nestjs-modules/mailer';
import { ConfigService } from '@nestjs/config';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    DatabaseModule,
    CoreModule,
    CVEModule,
    CPEModule,
    ProductsModule,
    MailerModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        transport: {
          service: 'gmail',
          host: 'smtp.gmail.com',
          port: 587,
          ignoreTLS: true,
          requireTLS: false,
          secure: false,
          auth: {
            user: configService.get('EMAIL_ADDRESS'),
            pass: configService.get('EMAIL_PASSWORD')
          }
        },
        defaults: {
          from: '"SVS" <svss.organization@gmail.com>'
        }
      })
    }),
    ScheduleModule.forRoot(),
    SeedModule
  ],
  controllers: [],
  providers: []
})
export class AppModule {}
