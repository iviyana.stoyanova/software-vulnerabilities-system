import { Module, HttpModule } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from '../database/entities/product.entity';
import { ProductsService } from './products.service';
import { User } from '../database/entities/user.entity';
import { ProductFormatterService } from '../utils/product-formatter.service';
import { CPE } from '../database/entities/cpe.entity';
import { CVEAssignment } from '../database/entities/cve.assignment.entity';
import { CVEService } from '../cve/cve.service';
import { ProductsMatchingService } from './products.matching.service';
import { EmailService } from '../utils/email.service';

@Module({
  imports: [TypeOrmModule.forFeature([Product, User, CPE, CVEAssignment]), HttpModule],
  providers: [
    ProductsService,
    ProductFormatterService,
    CVEService,
    ProductsMatchingService,
    EmailService
  ],
  exports: [ProductsService],
  controllers: [ProductsController]
})
export class ProductsModule {}
