import { ProductDTO } from '../../products/models/product.dto';
import { Expose } from 'class-transformer';
import { Optional } from '@nestjs/common';

export class ResponseUpdateProductDTO {
    @Expose()
    public message: string;

    @Optional()
    public data?: ProductDTO
}