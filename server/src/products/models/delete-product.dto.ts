import { Expose } from 'class-transformer';

export class DeleteProductDTO {
  @Expose()
  message: string
}