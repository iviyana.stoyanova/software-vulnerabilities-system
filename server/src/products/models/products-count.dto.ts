export class ProductsCount {
  all: number;
  withCVE: number;
  withoutCVE: number;
}
