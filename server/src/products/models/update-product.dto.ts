import { IsNotEmpty, IsString } from 'class-validator';

export class UpdateProductDTO {
    @IsNotEmpty()
    @IsString()
    public product: string;

    @IsNotEmpty()
    @IsString()
    public vendor: string;

    @IsNotEmpty()
    @IsString()
    public version: string;
}