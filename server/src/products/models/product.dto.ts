import { Expose, Type } from 'class-transformer';
import { CpeDTO } from '../../cpe/models/cpe.dto';

export class ProductDTO {
  @Expose()
  public id: number;

  @Expose()
  public product: string;

  @Expose()
  public vendor: string;

  @Expose()
  public version: string;

  @Expose()
  public username: string;

  @Expose()
  public hasCves: boolean;

  @Expose()
  public hasCpe: boolean;

  @Expose()
  public addedOn: string;

  @Type(() => CpeDTO)
  @Expose()
  public cpe: CpeDTO;
}
