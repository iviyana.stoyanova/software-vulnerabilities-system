import { IsString, IsDefined } from 'class-validator';

export class CreateProductDTO {
  @IsDefined()
  @IsString()
  public product: string;

  @IsDefined()
  @IsString()
  public vendor: string;

  @IsDefined()
  @IsString()
  public version: string;
}
