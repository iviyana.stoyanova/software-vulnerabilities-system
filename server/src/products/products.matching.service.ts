import { Injectable, BadRequestException } from '@nestjs/common';
import { Product } from '../database/entities/product.entity';
import { Repository, Like, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CPE } from '../database/entities/cpe.entity';
import { ProductsService } from './products.service';

@Injectable()
export class ProductsMatchingService {
  constructor(
    @InjectRepository(CPE)
    private readonly cpesRepository: Repository<CPE>,
    private readonly productsService: ProductsService
  ) {}

  public async matchProductToCPE(productId: number): Promise<CPE[]> {
    const foundProduct: Product = await this.productsService.productById(productId);
    if (foundProduct.hasCpe) {
      throw new BadRequestException('The product already has a CPE!');
    }

    const vendor = foundProduct.vendor
      .split(' ')
      .join('%')
      .toLowerCase();
    const version = foundProduct.version;
    const productName: string[] = foundProduct.product.toLowerCase().split(' ');
    const systemTypes = ['x64', 'x86'];
    let trimmedProductName = productName
      .filter((productTitle) => {
        return (
          productTitle !== foundProduct.vendor.toLowerCase() &&
          productTitle !== foundProduct.version.toLowerCase() &&
          !productTitle.includes('bit') &&
          !productTitle.includes(systemTypes[0]) &&
          !productTitle.includes(systemTypes[1])
        );
      })
      .join('%');
    if (!trimmedProductName) {
      trimmedProductName = productName.join('%');
    }
    const versionArray = version.split('.');
    const searchUriProduct = `cpe:2.3:a:%:${trimmedProductName}%`;
    const searchUriVendorAndProduct = `cpe:2.3:a:${vendor}:${trimmedProductName}%`;

    const foundCPEsByProduct: CPE[] = await this.findCPEsbyParameter(searchUriProduct);
    const foundCPEsByVendorAndProduct: CPE[] = await this.findCPEsbyParameter(
      searchUriVendorAndProduct
    );
    const isProductNameExisting = await this.checkIfProductNameExists(foundProduct.product);

    if (!isProductNameExisting && !foundCPEsByProduct.length) {
      const searchUriVendor = `cpe:2.3:a%${vendor}%`;
      const foundCPEsByVendor: CPE[] = await this.findCPEsbyParameter(searchUriVendor);

      return this.replaceDeprecatedCPEs(foundCPEsByVendor);
    }
    if (!foundCPEsByVendorAndProduct.length) {
      return this.replaceDeprecatedCPEs(foundCPEsByProduct);
    }

    let bestCandidates: CPE[];
    do {
      const searchUriVersion = `cpe:2.3:a:${vendor}:${trimmedProductName}%:${versionArray.join(
        '.'
      )}%`;
      const foundCPEsByVersion: CPE[] = await this.findCPEsbyParameter(searchUriVersion);

      if (foundCPEsByVersion.length) {
        bestCandidates = foundCPEsByVersion;
        break;
      } else {
        versionArray.pop();
      }
    } while (versionArray.length);

    if (!bestCandidates) {
      return this.replaceDeprecatedCPEs(foundCPEsByVendorAndProduct);
    }

    if (
      foundProduct.product.toLowerCase().includes(systemTypes[0]) ||
      foundProduct.product.toLowerCase().includes(systemTypes[1])
    ) {
      const systemType = foundProduct.product.toLowerCase().includes(systemTypes[0])
        ? systemTypes[0]
        : systemTypes[1];
      const searchUriSystemType = `cpe:2.3:a:${vendor}:${trimmedProductName}%:${versionArray.join(
        '.'
      )}%${systemType}%`;

      const foundCPEsBySystemType: CPE[] = await this.findCPEsbyParameter(searchUriSystemType);

      return this.replaceDeprecatedCPEs(foundCPEsBySystemType);
    } else {
      return this.replaceDeprecatedCPEs(bestCandidates);
    }
  }

  private async findCPEsbyParameter(searchUri: string): Promise<CPE[]> {
    const foundCPEsByParameter: CPE[] = await this.cpesRepository.find({
      where: {
        cpeUri: Like(searchUri)
      },
      select: ['title', 'lastModifiedDate', 'cpeUri', 'deprecatedBy']
    });

    return foundCPEsByParameter;
  }

  private async checkIfProductNameExists(product: string): Promise<boolean> {
    const CPEsWithFoundProductName: CPE[] = await this.cpesRepository.find({
      where: {
        cpeUri: Like(`%${product.toLowerCase()}%`)
      },
      select: ['title']
    });

    return CPEsWithFoundProductName.length > 0 ? true : false;
  }

  private async replaceDeprecatedCPEs(cpes: CPE[]): Promise<CPE[]> {
    const result = await Promise.all(
      cpes.map(async (cpe) => {
        if (!cpe.deprecatedBy) {
          return [cpe];
        } else {
          const cpeUris: string[] = JSON.parse(cpe.deprecatedBy);
          const deprecatedByCPEs: CPE[] = await this.cpesRepository.find({
            where: {
              cpeUri: In(cpeUris)
            },
            select: ['title', 'lastModifiedDate', 'cpeUri', 'deprecatedBy']
          });
          return this.replaceDeprecatedCPEs(deprecatedByCPEs);
        }
      })
    );
    const replacedCPEs: CPE[] = [].concat(...result);
    const replacedCPEUris: string[] = replacedCPEs.map((cpe) => cpe.cpeUri);

    return replacedCPEs.filter((cpe, index) => replacedCPEUris.indexOf(cpe.cpeUri) === index);
  }
}
