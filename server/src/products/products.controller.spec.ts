import { ProductsController } from './products.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { ProductsService } from './products.service';
import { SortType } from '../enums/sort-type.enum';
import { SortBy } from '../enums/sort-by.enum';
import { ProductsMatchingService } from './products.matching.service';
import { Product } from '../database/entities/product.entity';
import { DataPaginationDTO } from '../cve/models/data-pagination.dto';
import { User } from '../database/entities/user.entity';
import { CPE } from '../database/entities/cpe.entity';
import { CVEAssignment } from '../database/entities/cve.assignment.entity';
import { UserDTO } from '../users/models/user.dto';
import { async } from 'rxjs/internal/scheduler/async';
import { CreateProductDTO } from './models/create-product.dto';
import { ProductDTO } from './models/product.dto';
import { CpeDTO } from '../cpe/models/cpe.dto';
import { ProductsCount } from './models/products-count.dto';

describe('ProductsController', () => {
  let controller: ProductsController;

  let productsService: any;
  let productsMatchingService: any;

  beforeEach(async () => {
    productsService = {
      allProducts() {
        /* empty */
      },
      addProduct() {
        /* empty */
      },
      allProductsWithCVEs() {
        /* empty */
      },
      allProductsWithoutCVEs() {
        /* empty */
      },
      getProductsCount() {
        /* empty */
      },
      productByIdDTO() {
        /* empty */
      },
      updateProduct() {
        /* empty */
      },
      deleteProduct() {
        /* empty */
      },
      searchByVendor() {
        /* empty */
      },
      searchByProduct() {
        /* empty */
      },
      searchByCPE() {
        /* empty */
      },
      searchByCVE() {
        /* empty */
      },
      allCvesForProduct() {
        /* empty */
      },
      assignCPE() {
        /* empty */
      },
      assignCVE() {
        /* empty */
      },
      unassignCVE() {
        /* empty */
      },
      matchCPEs() {
        /* empty */
      },
      productById() {
        /* empty */
      },
      sendEmail() {
        /* empty */
      }
    };
    productsMatchingService = {
      matchProductToCPE() {
        /* empty */
      }
    };

    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProductsController],
      providers: [
        {
          provide: ProductsService,
          useValue: productsService
        },
        {
          provide: ProductsMatchingService,
          useValue: productsMatchingService
        }
      ]
    }).compile();

    controller = module.get<ProductsController>(ProductsController);
  });

  describe('allProducts()', () => {
    it('should call productsService allProducts() once with correct parameter', async () => {
      // Arrange
      const mockQuery = { page: 1, perPage: 10, sortBy: SortBy.DATE, sortType: SortType.ASC };
      const spy = jest.spyOn(controller, 'allProducts');

      // Act
      await controller.allProducts(mockQuery);

      // Assert
      expect(spy).toBeCalledWith(mockQuery);
      expect(spy).toBeCalledTimes(1);
    });
    it('should call productsService allProducts() and return correct data', async () => {
      // Arrange
      const mockProducts: Product[] = [
        {
          id: 1,
          product: 'Firefox',
          vendor: 'Mozilla',
          version: '72.0.2',
          hasCpe: true,
          hasCves: true,
          addedOn: '2020-06-05 22:09:09',
          isDeleted: false,
          user: Promise.resolve(new User()),
          cpe: Promise.resolve(new CPE()),
          cves: Promise.all([new CVEAssignment()])
        }
      ];
      const expectedResult: DataPaginationDTO<Product[]> = {
        resultsPerPage: 1,
        page: 1,
        totalResults: 1,
        data: mockProducts
      };
      const mockQuery = { page: 1, perPage: 10, sortBy: SortBy.DATE, sortType: SortType.ASC };

      jest.spyOn(productsService, 'allProducts').mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: DataPaginationDTO<Product[]> = await controller.allProducts(mockQuery);

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });

  describe('addProduct()', () => {
    it('should call productsService addProduct() once with correct parameter', async () => {
      // Arrange
      const mockBody: CreateProductDTO = new CreateProductDTO();
      const mockUser: UserDTO = new UserDTO();
      const spy = jest.spyOn(controller, 'addProduct');

      // Act
      await controller.addProduct(mockBody, mockUser);

      // Assert
      expect(spy).toBeCalledWith(mockBody, mockUser);
      expect(spy).toBeCalledTimes(1);
    });

    it('should call productsService addProduct() and return correct data', async () => {
      // Arrange
      const mockBody: CreateProductDTO = new CreateProductDTO();
      const mockUser: UserDTO = new UserDTO();
      const expectedResult: ProductDTO = {
        id: 1,
        product: 'Firefox',
        vendor: 'Mozilla',
        version: '72.0.2',
        username: 'SVS',
        hasCpe: true,
        hasCves: true,
        addedOn: '2020-06-05 22:09:09',
        cpe: new CpeDTO()
      };
      jest.spyOn(productsService, 'addProduct').mockReturnValue(Promise.resolve(expectedResult));

      // Act
      // await controller.addProduct(mockBody, mockUser);
      const actualResult: ProductDTO = await controller.addProduct(mockBody, mockUser);

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });

  describe('allProductsWithCVEs()', () => {
    it('should call productsService allProductsWithCVEs() once with correct parameter', async () => {
      // Arrange
      const mockQuery = { page: 1, perPage: 10, sortBy: SortBy.DATE, sortType: SortType.ASC };
      const spy = jest.spyOn(controller, 'allProductsWithCVEs');

      // Act
      await controller.allProductsWithCVEs(mockQuery);

      // Assert
      expect(spy).toBeCalledWith(mockQuery);
      expect(spy).toBeCalledTimes(1);
    });

    it('should call productsService allProductsWithCVEs() and return correct data', async () => {
      const mockProducts: Product[] = [
        {
          id: 1,
          product: 'Firefox',
          vendor: 'Mozilla',
          version: '72.0.2',
          hasCpe: true,
          hasCves: true,
          addedOn: '2020-06-05 22:09:09',
          isDeleted: false,
          user: Promise.resolve(new User()),
          cpe: Promise.resolve(new CPE()),
          cves: Promise.all([new CVEAssignment()])
        }
      ];
      const expectedResult: DataPaginationDTO<Product[]> = {
        resultsPerPage: 1,
        page: 1,
        totalResults: 1,
        data: mockProducts
      };
      const mockQuery = { page: 1, perPage: 10, sortBy: SortBy.DATE, sortType: SortType.ASC };

      jest
        .spyOn(productsService, 'allProductsWithCVEs')
        .mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: DataPaginationDTO<Product[]> = await controller.allProductsWithCVEs(
        mockQuery
      );

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });
  describe('allProductsWithoutCVEs()', () => {
    it('should call productsService allProductsWithoutCVEs() once with correct parameter', async () => {
      // Arrange
      const mockQuery = { page: 1, perPage: 10, sortBy: SortBy.DATE, sortType: SortType.ASC };
      const spy = jest.spyOn(controller, 'allProductsWithoutCVEs');

      // Act
      await controller.allProductsWithoutCVEs(mockQuery);

      // Assert
      expect(spy).toBeCalledWith(mockQuery);
      expect(spy).toBeCalledTimes(1);
    });

    it('should call productsService allProductsWithoutCVEs() and return correct data', async () => {
      const mockProducts: Product[] = [
        {
          id: 1,
          product: 'Firefox',
          vendor: 'Mozilla',
          version: '72.0.2',
          hasCpe: true,
          hasCves: true,
          addedOn: '2020-06-05 22:09:09',
          isDeleted: false,
          user: Promise.resolve(new User()),
          cpe: Promise.resolve(new CPE()),
          cves: Promise.all([new CVEAssignment()])
        }
      ];
      const expectedResult: DataPaginationDTO<Product[]> = {
        resultsPerPage: 1,
        page: 1,
        totalResults: 1,
        data: mockProducts
      };
      const mockQuery = { page: 1, perPage: 10, sortBy: SortBy.DATE, sortType: SortType.ASC };

      jest
        .spyOn(productsService, 'allProductsWithoutCVEs')
        .mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: DataPaginationDTO<Product[]> = await controller.allProductsWithoutCVEs(
        mockQuery
      );

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });

  describe('productById()', () => {
    it('should call productsService productByIdDTO() once with correct data', async () => {
      // Arrange
      const mockProductId = '1';
      const mockResult: ProductDTO = new ProductDTO();
      const spy = jest
        .spyOn(productsService, 'productByIdDTO')
        .mockReturnValue(Promise.resolve(mockResult));

      // Act
      await controller.productById(mockProductId);

      // Assert
      expect(spy).toBeCalledTimes(1);
    });

    it('should call productsService productById() and return correct data', async () => {
      // Arrange
      const mockProductId = '1';
      const expectedResult: ProductDTO = {
        id: 1,
        product: 'Firefox',
        vendor: 'Mozilla',
        version: '72.0.2',
        username: 'SVS',
        hasCpe: true,
        hasCves: true,
        addedOn: '2020-06-05 22:09:09',
        cpe: new CpeDTO()
      };

      jest
        .spyOn(productsService, 'productByIdDTO')
        .mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: ProductDTO = await controller.productById(mockProductId);

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });

  describe('productsCount()', () => {
    it('should call productsService getProductsCount() once', async () => {
      // Arrange
      const mockResult: ProductsCount = new ProductsCount();
      const spy = jest
        .spyOn(productsService, 'getProductsCount')
        .mockReturnValue(Promise.resolve(mockResult));

      // Act
      await controller.productsCount();

      // Assert
      expect(spy).toBeCalledTimes(1);
    });

    it('should call productsService getProductsCount() and return correct data', async () => {
      // Arrange
      const expectedResult: ProductsCount = {
        all: 10,
        withCVE: 5,
        withoutCVE: 5
      };

      jest
        .spyOn(productsService, 'getProductsCount')
        .mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: ProductsCount = await controller.productsCount();

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });

  describe('updateProduct()', () => {
    it('should call productsService updateProduct() once with correct data', async () => {
      // Arrange
      const mockProductId = '1';
      const mockResult: ProductDTO = new ProductDTO();
      const spy = jest
        .spyOn(productsService, 'updateProduct')
        .mockReturnValue(Promise.resolve(mockResult));

      // Act
      await controller.updateProduct(mockProductId, mockResult);

      // Assert
      expect(spy).toBeCalledTimes(1);
    });

    it('should call productsService updateProduct() and return correct data', async () => {
      // Arrange
      const mockProductId = '1';
      const mockResult: ProductDTO = new ProductDTO();
      const expectedResult: Partial<ProductDTO> = {
        version: '83.0.4103.61'
      };

      jest.spyOn(productsService, 'updateProduct').mockReturnValue(Promise.resolve(expectedResult));

      // Act
      const actualResult: Partial<ProductDTO> = await controller.updateProduct(
        mockProductId,
        mockResult
      );

      // Assert
      expect(expectedResult).toEqual(actualResult);
    });
  });

  describe('sendEmail()', () => {
    it('should call productsService sendEmail() once with correct data', async () => {
      // Arrange
      const mockProductId = '1';
      const mockProduct: Partial<ProductDTO> = new ProductDTO();
      const mockUser: UserDTO = new UserDTO();
      const spy = jest.spyOn(productsService, 'sendEmail').mockReturnValue(Promise.resolve());

      // Act
      await controller.sendEmail(mockUser, mockProduct, mockProductId);

      // Assert
      expect(spy).toBeCalledTimes(1);
    });
  });
});
