import { UpdateProductDTO } from './models/update-product.dto';
import { CveDTO } from './../cve/models/cve.dto';
import { CVEsForProductDTO } from './../cve/models/cves.for.product.dto';
import {
  Injectable,
  NotFoundException,
  BadRequestException,
  HttpService,
  InternalServerErrorException
} from '@nestjs/common';
import { Product } from '../database/entities/product.entity';
import { Repository, Like } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateProductDTO } from './models/create-product.dto';
import { UserDTO } from '../users/models/user.dto';
import { User } from '../database/entities/user.entity';
import { ProductDTO } from './models/product.dto';
import { ProductFormatterService } from '../utils/product-formatter.service';
import { DataPaginationDTO } from '../cve/models/data-pagination.dto';
import { SortBy } from '../enums/sort-by.enum';
import { SortType } from '../enums/sort-type.enum';
import { ProductsCount } from './models/products-count.dto';
import { plainToClass } from 'class-transformer';
import { CPE } from '../database/entities/cpe.entity';
import { CVEAssignment } from '../database/entities/cve.assignment.entity';
import { CVEService } from '../cve/cve.service';
import { EmailService } from '../utils/email.service';
import { UserRole } from '../enums/user-role.enum';
@Injectable()
export class ProductsService {
  private readonly cvesUrl: string = 'https://services.nvd.nist.gov/rest/json/cves/1.0';

  constructor(
    @InjectRepository(Product)
    private readonly productsRepository: Repository<Product>,
    @InjectRepository(User)
    private readonly usersRepository: Repository<User>,
    private readonly httpService: HttpService,
    @InjectRepository(CPE)
    private readonly cpesRepository: Repository<CPE>,
    @InjectRepository(CVEAssignment)
    private readonly cveAssignmentRepository: Repository<CVEAssignment>,
    private readonly formatterService: ProductFormatterService,
    private readonly cvesService: CVEService,
    private readonly emailService: EmailService
  ) {}

  public async allProductsWithCVEs(
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<Product[]>> {
    const pageIndex: number = resultsPerPage * (page - 1);
    const [products, totalResults]: [
      Product[],
      number
    ] = await this.productsRepository.findAndCount({
      order: { [sortBy]: sortType },
      where: {
        hasCves: true,
        isDeleted: false
      },
      select: ['id', 'product', 'vendor', 'version', 'hasCves', 'hasCpe', 'addedOn'],
      skip: pageIndex,
      take: resultsPerPage
    });

    const dataWithPagination: DataPaginationDTO<Product[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: products
    };

    return dataWithPagination;
  }

  public async allProductsWithoutCVEs(
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<Product[]>> {
    const pageIndex: number = resultsPerPage * (page - 1);
    const [products, totalResults]: [
      Product[],
      number
    ] = await this.productsRepository.findAndCount({
      order: { [sortBy]: sortType },
      where: {
        hasCves: false,
        isDeleted: false
      },
      select: ['id', 'product', 'vendor', 'version', 'hasCves', 'hasCpe', 'addedOn'],
      skip: pageIndex,
      take: resultsPerPage
    });

    const dataWithPagination: DataPaginationDTO<Product[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: products
    };

    return dataWithPagination;
  }

  public async getProductsCount(): Promise<ProductsCount> {
    const withCVE: number = await this.productsRepository.count({
      where: {
        isDeleted: false,
        hasCves: true
      }
    });
    const withoutCVE: number = await this.productsRepository.count({
      where: {
        isDeleted: false,
        hasCves: false
      }
    });
    const countInfo: ProductsCount = { withCVE, withoutCVE, all: withCVE + withoutCVE };

    return countInfo;
  }

  public async allProducts(
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<Partial<Product[]>>> {
    const pageIndex: number = resultsPerPage * (page - 1);
    const [products, totalResults]: [
      Product[],
      number
    ] = await this.productsRepository.findAndCount({
      where: { isDeleted: false },
      select: ['id', 'product', 'vendor', 'version', 'hasCves', 'hasCpe', 'addedOn'],
      order: { [sortBy]: sortType },
      skip: pageIndex,
      take: resultsPerPage
    });

    const dataWithPagination: DataPaginationDTO<Product[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: products
    };

    return dataWithPagination;
  }

  public async addProduct(product: CreateProductDTO, user: UserDTO): Promise<ProductDTO> {
    const foundUser: User = await this.usersRepository.findOne({
      username: user.username
    });

    const foundProduct = await this.productsRepository
      .createQueryBuilder()
      .where('LOWER(product) = LOWER(:product)', { product: product.product })
      .andWhere('LOWER(vendor) = LOWER(:vendor)', { vendor: product.vendor })
      .andWhere('LOWER(version) = LOWER(:version)', { version: product.version })
      .getOne();

    if (foundProduct) {
      throw new BadRequestException('The product already exist in the Database!');
    }
    const productEntity: Product = this.productsRepository.create(product);
    productEntity.user = Promise.resolve(foundUser);
    await this.productsRepository.save(productEntity);

    return this.formatterService.productUsernameFormatter(productEntity);
  }

  public async productByIdDTO(productId: number): Promise<ProductDTO> {
    const foundProduct: Product = await this.productById(productId);
    return this.formatterService.productDetailsFormatter(foundProduct);
  }

  public async updateProduct(
    productId: number,
    product: Partial<UpdateProductDTO>
  ): Promise<ProductDTO> {
    const oldProduct: Product = await this.productById(productId);

    const productToUpdate: Product = {
      ...oldProduct,
      ...product
    };
    await this.productsRepository.save(productToUpdate);
    const savedProduct: Product = await this.productById(productId);

    return this.formatterService.productDetailsFormatter(savedProduct);
  }

  public async deleteProduct(productId: number): Promise<ProductDTO> {
    const foundProduct: Product = await this.productById(productId);
    foundProduct.isDeleted = true;
    foundProduct.cpe = Promise.resolve(null);
    foundProduct.hasCpe = false;
    foundProduct.hasCves = false;
    const savedProduct: Product = await this.productsRepository.save(foundProduct);
    await this.cveAssignmentRepository.remove(await savedProduct.cves);

    return this.formatterService.productDetailsFormatter(savedProduct);
  }

  public async searchByVendor(
    vendor: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<Product[]>> {
    const searchString = this.replaceWildcards(vendor);
    const startIndex: number = resultsPerPage * (page - 1);
    const [products, totalResults]: [
      Product[],
      number
    ] = await this.productsRepository.findAndCount({
      where: {
        isDeleted: false,
        vendor: Like(searchString)
      },
      select: ['id', 'product', 'vendor', 'version', 'hasCves', 'addedOn'],
      order: { [sortBy]: sortType },
      skip: startIndex,
      take: resultsPerPage
    });

    const result: DataPaginationDTO<Product[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: products
    };

    return result;
  }

  public async searchByProduct(
    product: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<Product[]>> {
    const searchString = this.replaceWildcards(product);
    const startIndex: number = resultsPerPage * (page - 1);
    const [products, totalResults]: [
      Product[],
      number
    ] = await this.productsRepository.findAndCount({
      where: {
        isDeleted: false,
        product: Like(searchString)
      },
      select: ['id', 'product', 'vendor', 'version', 'hasCves', 'addedOn'],
      order: { [sortBy]: sortType },
      skip: startIndex,
      take: resultsPerPage
    });

    const result: DataPaginationDTO<Product[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: products
    };

    return result;
  }

  public async searchByCPE(
    cpe: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<Product[]>> {
    const searchString = this.replaceWildcards(cpe);
    const startIndex: number = resultsPerPage * (page - 1);
    const foundProducts: Product[] = await this.productsRepository
      .createQueryBuilder('prod')
      .leftJoin('prod.cpe', 'cpe')
      .select('prod.product', 'product')
      .addSelect('prod.id', 'id')
      .addSelect('prod.vendor', 'vendor')
      .addSelect('prod.version', 'version')
      .addSelect('prod.hasCves', 'hasCves')
      .addSelect('prod.addedOn', 'addedOn')
      .where(
        '(cpe.cpeUri LIKE (:searchCPE) OR cpe.title LIKE (:searchCPE)) AND prod.isDeleted = false',
        {
          searchCPE: searchString
        }
      )
      .orderBy(`prod.${sortBy}`, sortType)
      .skip(startIndex)
      .take(resultsPerPage)
      .getRawMany();
    const totalResults: number = await this.productsRepository
      .createQueryBuilder('prod')
      .leftJoin('prod.cpe', 'cpe')
      .where(
        '(cpe.cpeUri LIKE (:searchCPE) OR cpe.title LIKE (:searchCPE)) AND prod.isDeleted = false',
        {
          searchCPE: searchString
        }
      )
      .getCount();

    const result: DataPaginationDTO<Product[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: foundProducts
    };

    return result;
  }

  public async searchByCVE(
    cve: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Promise<DataPaginationDTO<Product[]>> {
    const searchString = this.replaceWildcards(cve);
    const startIndex: number = resultsPerPage * (page - 1);
    const foundProducts: Product[] = await this.productsRepository
      .createQueryBuilder('prod')
      .leftJoin('prod.cves', 'cve')
      .select('prod.product', 'product')
      .addSelect('prod.id', 'id')
      .addSelect('prod.vendor', 'vendor')
      .addSelect('prod.version', 'version')
      .addSelect('prod.hasCves', 'hasCves')
      .addSelect('prod.addedOn', 'addedOn')
      .where('cve.cveId LIKE (:searchCVE) AND prod.isDeleted = false', { searchCVE: searchString })
      .orderBy(`prod.${sortBy}`, sortType)
      .skip(startIndex)
      .take(resultsPerPage)
      .getRawMany();
    const totalResults: number = await this.productsRepository
      .createQueryBuilder('prod')
      .leftJoin('prod.cves', 'cve')
      .where('cve.cveId LIKE (:searchCVE) AND prod.isDeleted = false', {
        searchCVE: searchString
      })
      .getCount();

    const result: DataPaginationDTO<Product[]> = {
      resultsPerPage,
      page,
      totalResults,
      data: foundProducts
    };

    return result;
  }

  public async allCVEsForProduct(productId: number): Promise<CVEsForProductDTO> {
    const foundProduct: Product = await this.productById(productId);
    const productCPE: CPE = await foundProduct.cpe;

    if (!productCPE) {
      throw new BadRequestException('There is no CPE assigned for this product!');
    }
    let startIndex = 0;
    let totalResults: number;
    const maxResultsPerPage = 200;
    const result: CveDTO[] = [];
    do {
      const data: any = await this.getCVEsStartingFrom(
        startIndex,
        maxResultsPerPage,
        productCPE.cpeUri
      );
      const cvesList: CveDTO[] = data.result.CVE_Items.map((foundCve) => {
        return plainToClass(CveDTO, foundCve, {
          excludeExtraneousValues: true
        });
      });
      result.push(...cvesList);
      totalResults = data.totalResults;
      startIndex += maxResultsPerPage;
      await new Promise((resolve) => setTimeout(resolve, 1000));
    } while (startIndex < totalResults - 1);
    const assignedCVEIds: string[] = (await foundProduct.cves).map((cve) => cve.cveId);
    const assignedCVEs: CveDTO[] = result.filter((cve) => assignedCVEIds.includes(cve.cveId));
    const unassignedCVEs: CveDTO[] = result.filter((cve) => !assignedCVEIds.includes(cve.cveId));
    const allCVEs: CVEsForProductDTO = { assignedCVEs, unassignedCVEs };

    return allCVEs;
  }

  private async getCVEsStartingFrom(
    index: number,
    maxResultsPerPage: number,
    cpeUri: string
  ): Promise<any> {
    const url = `${this.cvesUrl}?startIndex=${index}&resultsPerPage=${maxResultsPerPage}&cpeMatchString=${cpeUri}`;
    const res = await this.httpService
      .get(url, { maxContentLength: 10000000 })
      .toPromise()
      .catch(() => {
        throw new InternalServerErrorException('Error while connecting to the NVD API!');
      });
    if (typeof res.data === 'string') {
      return JSON.parse(res.data);
    }
    if (index === 1500) {
      console.log(res.data.length);
    }

    return res.data;
  }

  public async assignCPE(productId: number, cpeUri: string): Promise<ProductDTO> {
    const foundProduct: Product = await this.productById(productId);
    if (foundProduct.hasCpe) {
      throw new BadRequestException('The product already has a CPE!');
    }
    const foundCPE: CPE = await this.cpesRepository.findOne({ where: { cpeUri } });

    if (!foundCPE) {
      throw new NotFoundException('The CPE does not exist in the Database!');
    }
    if (await foundCPE.product) {
      throw new BadRequestException('The CPE has already assign to a product!');
    }

    const updatedProduct: Product = await this.productsRepository.create({
      ...foundProduct,
      hasCpe: true
    });

    updatedProduct.cpe = Promise.resolve(foundCPE);
    await this.productsRepository.save(updatedProduct);

    return this.formatterService.productDetailsFormatter(updatedProduct);
  }

  public async assignCVE(productId: number, cveId: string): Promise<ProductDTO> {
    const foundProduct: Product = await this.productById(productId);
    if (!foundProduct.hasCpe) {
      throw new BadRequestException('The product does not have a CPE and you cannot assign a CVE!');
    }

    const foundCVEfromDatabase: CVEAssignment = await this.cveAssignmentRepository.findOne({
      where: { cveId }
    });
    if (foundCVEfromDatabase) {
      throw new BadRequestException('The CVE has already been assigned to a product!');
    }

    await this.cvesService.cveById(cveId).toPromise();

    const newCVE = await this.cveAssignmentRepository.create({ cveId });

    const updatedProduct: Product = await this.productsRepository.create({
      ...foundProduct,
      hasCves: true
    });

    newCVE.product = Promise.resolve(updatedProduct);
    (await updatedProduct.cves).push(newCVE);
    await this.cveAssignmentRepository.save(newCVE);
    await this.productsRepository.save(updatedProduct);

    return this.formatterService.productDetailsFormatter(updatedProduct);
  }

  public async unassignCVE(productId: number, cveId: string): Promise<void> {
    const foundProduct: Product = await this.productById(productId);
    if (!foundProduct.hasCpe) {
      throw new BadRequestException('The product does not have a CPE and you cannot assign a CVE!');
    }

    const foundCVEfromDatabase: CVEAssignment = await this.cveAssignmentRepository.findOne({
      where: { cveId }
    });
    if (!foundCVEfromDatabase) {
      throw new BadRequestException('The CVE has not been assigned to the product!');
    }
    const assignedCVEs: CVEAssignment[] = await foundProduct.cves;
    await this.cveAssignmentRepository.delete(foundCVEfromDatabase);
    console.log(assignedCVEs);
    if (assignedCVEs.length === 1) {
      foundProduct.hasCves = false;
      await this.productsRepository.save(foundProduct);
    }
  }

  public async productById(productId: number): Promise<Product> {
    const foundProduct: Product = await this.productsRepository.findOne({
      relations: ['cpe', 'user'],
      where: { id: productId, isDeleted: false }
    });
    if (!foundProduct) {
      throw new NotFoundException('The product does not exist in the Database!');
    }

    return foundProduct;
  }

  public async sendEmail(
    user: UserDTO,
    product: Partial<ProductDTO>,
    productId: string
  ): Promise<void> {
    const foundAdmins: User[] = await this.usersRepository.find({
      where: { role: UserRole.ADMIN, isDeleted: false }
    });

    const productCVEs: CVEsForProductDTO = await this.allCVEsForProduct(+productId);
    const asignedCVEs: CveDTO[] = await productCVEs.assignedCVEs;

    const assignedCVEsFormatting: string[] = asignedCVEs.map((cve) => {
      return `CVE-ID: ${cve.cveId}
              Description: ${cve.description}
              Last modified date: ${cve.lastModifiedDate}
              Severity: ${cve.severity}

      `;
    });

    const productEntity: Product = await this.productById(+productId);
    const productCPE: CPE = await productEntity.cpe;

    foundAdmins.forEach((admin) => {
      this.emailService.sendMail(
        admin.email,
        'New vulnerabilities assigned to a product',
        `Product ${product.product} has new vulnerabilities assigned by ${user.username}.
         Vendor: ${product.vendor}
         Version: ${product.version}

         CPE Title: ${productCPE.title}
         CPE Uri: ${productCPE.cpeUri}

         CVEs:
         ${assignedCVEsFormatting.join('')}
        `
      );
    });
  }

  private replaceWildcards(searchString: string): string {
    return `${searchString
      .replace('%', '\\%')
      .replace('_', '\\_')
      .replace('*', '%')}%`;
  }
}
