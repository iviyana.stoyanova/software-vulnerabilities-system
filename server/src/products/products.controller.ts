import { CPE } from './../database/entities/cpe.entity';
import { CVEsForProductDTO } from './../cve/models/cves.for.product.dto';
import { DeleteProductDTO } from './models/delete-product.dto';
import { AdminGuard } from './../common/guards/admin.guard';
import {
  Controller,
  Get,
  HttpStatus,
  HttpCode,
  Post,
  Body,
  UseGuards,
  Query,
  Param,
  Delete,
  Put
} from '@nestjs/common';
import { ProductsService } from './products.service';
import { Product } from '../database/entities/product.entity';
import { CreateProductDTO } from './models/create-product.dto';
import { UserDTO } from '../users/models/user.dto';
import { User } from '../common/decorators/user.decorator';
import { AuthGuardWithBlacklisting } from '../common/guards/blacklist.guard';
import { ProductDTO } from './models/product.dto';
import { DataPaginationDTO } from '../cve/models/data-pagination.dto';
import { SortType } from '../enums/sort-type.enum';
import { SortBy } from '../enums/sort-by.enum';
import { ProductsCount } from './models/products-count.dto';
import { ProductsMatchingService } from './products.matching.service';

@Controller('products')
@UseGuards(AuthGuardWithBlacklisting)
export class ProductsController {
  constructor(
    private readonly productsService: ProductsService,
    private readonly productsMatchingService: ProductsMatchingService
  ) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  public async allProducts(@Query() query): Promise<DataPaginationDTO<Product[]>> {
    let { page, resultsPerPage, sortBy, sortType } = query;
    page = page || 1;
    resultsPerPage = resultsPerPage || 10;
    sortBy = sortBy || SortBy.DATE;
    sortType = sortType || SortType.ASC;

    return this.productsService.allProducts(page, resultsPerPage, sortBy, sortType);
  }

  @Post()
  @HttpCode(HttpStatus.CREATED)
  public async addProduct(
    @Body() body: CreateProductDTO,
    @User() user: UserDTO
  ): Promise<ProductDTO> {
    return this.productsService.addProduct(body, user);
  }

  @Get('cves')
  @HttpCode(HttpStatus.OK)
  public async allProductsWithCVEs(@Query() query): Promise<DataPaginationDTO<Product[]>> {
    let { page, resultsPerPage, sortBy, sortType } = query;
    page = page || 1;
    resultsPerPage = resultsPerPage || 10;
    sortBy = sortBy || SortBy.DATE;
    sortType = sortType || SortType.ASC;

    return this.productsService.allProductsWithCVEs(page, resultsPerPage, sortBy, sortType);
  }

  @Get('without-cves')
  @HttpCode(HttpStatus.OK)
  public async allProductsWithoutCVEs(@Query() query): Promise<DataPaginationDTO<Product[]>> {
    let { page, resultsPerPage, sortBy, sortType } = query;
    page = page || 1;
    resultsPerPage = resultsPerPage || 10;
    sortBy = sortBy || SortBy.DATE;
    sortType = sortType || SortType.ASC;

    return this.productsService.allProductsWithoutCVEs(page, resultsPerPage, sortBy, sortType);
  }

  @Get('number-of-products')
  @HttpCode(HttpStatus.OK)
  public async productsCount(): Promise<ProductsCount> {
    return this.productsService.getProductsCount();
  }

  @Get('/:productId')
  @HttpCode(HttpStatus.OK)
  public async productById(@Param('productId') productId: string): Promise<ProductDTO> {
    return this.productsService.productByIdDTO(+productId);
  }

  @Put('/:productId')
  @HttpCode(HttpStatus.OK)
  public async updateProduct(
    @Param('productId') productId: string,
    @Body() product: Partial<ProductDTO>
  ): Promise<ProductDTO> {
    return this.productsService.updateProduct(+productId, product);
  }

  @Delete('/:productId')
  @UseGuards(AdminGuard)
  public async deleteProduct(@Param('productId') productId: string): Promise<DeleteProductDTO> {
    const deletedProduct: ProductDTO = await this.productsService.deleteProduct(+productId);
    return { message: `${deletedProduct.product} has been removed successfully!` };
  }

  @Get('/search/byVendor')
  @HttpCode(HttpStatus.OK)
  public async searchByVendor(@Query() query): Promise<DataPaginationDTO<Product[]>> {
    let { page, resultsPerPage, sortBy, sortType, searchString } = query;
    page = page || 1;
    resultsPerPage = resultsPerPage || 10;
    sortBy = sortBy || SortBy.DATE;
    sortType = sortType || SortType.ASC;
    searchString = searchString || '';

    return this.productsService.searchByVendor(
      searchString,
      page,
      resultsPerPage,
      sortBy,
      sortType
    );
  }

  @Get('/search/byProduct')
  @HttpCode(HttpStatus.OK)
  public async searchByProduct(@Query() query): Promise<DataPaginationDTO<Product[]>> {
    let { page, resultsPerPage, sortBy, sortType, searchString } = query;
    page = page || 1;
    resultsPerPage = resultsPerPage || 10;
    sortBy = sortBy || SortBy.DATE;
    sortType = sortType || SortType.ASC;
    searchString = searchString || '';

    return this.productsService.searchByProduct(
      searchString,
      page,
      resultsPerPage,
      sortBy,
      sortType
    );
  }

  @Get('/search/byCPE')
  @HttpCode(HttpStatus.OK)
  public async searchByCPE(@Query() query): Promise<DataPaginationDTO<Product[]>> {
    let { page, resultsPerPage, sortBy, sortType, searchString } = query;
    page = page || 1;
    resultsPerPage = resultsPerPage || 10;
    sortBy = sortBy || SortBy.DATE;
    sortType = sortType || SortType.ASC;
    searchString = searchString || '';

    return this.productsService.searchByCPE(searchString, page, resultsPerPage, sortBy, sortType);
  }

  @Get('/search/byCVE')
  @HttpCode(HttpStatus.OK)
  public async searchByCVE(@Query() query): Promise<DataPaginationDTO<Product[]>> {
    let { page, resultsPerPage, sortBy, sortType, searchString } = query;
    page = page || 1;
    resultsPerPage = resultsPerPage || 10;
    sortBy = sortBy || SortBy.DATE;
    sortType = sortType || SortType.ASC;
    searchString = searchString || '';

    return this.productsService.searchByCVE(searchString, page, resultsPerPage, sortBy, sortType);
  }

  @Get('/:productId/cves')
  @HttpCode(HttpStatus.OK)
  public async allCvesForProduct(
    @Param('productId') productId: string
  ): Promise<CVEsForProductDTO> {
    return this.productsService.allCVEsForProduct(+productId);
  }

  @Put('/:productId/cpe/:cpeUri')
  @HttpCode(HttpStatus.OK)
  public async assignCPE(
    @Param('productId') productId: string,
    @Param('cpeUri') cpeUri: string
  ): Promise<ProductDTO> {
    return this.productsService.assignCPE(+productId, cpeUri);
  }

  @Put('/:productId/cves/:cveId')
  @HttpCode(HttpStatus.OK)
  public async assignCVE(
    @Param('productId') productId: string,
    @Param('cveId') cveId: string
  ): Promise<ProductDTO> {
    return this.productsService.assignCVE(+productId, cveId);
  }

  @Delete('/:productId/cves/:cveId')
  @HttpCode(HttpStatus.OK)
  public async unassignCVE(
    @Param('productId') productId: string,
    @Param('cveId') cveId: string
  ): Promise<void> {
    return this.productsService.unassignCVE(+productId, cveId);
  }

  @Get('/:productId/matchingCPEs')
  @HttpCode(HttpStatus.OK)
  public async matchCPEs(@Query() query, @Param('productId') productId: string): Promise<CPE[]> {
    return this.productsMatchingService.matchProductToCPE(+productId);
  }

  @Post(':productId/email')
  @HttpCode(HttpStatus.CREATED)
  public async sendEmail(
    @User() user: UserDTO,
    @Body() product: Partial<ProductDTO>,
    @Param('productId') productId: string
  ): Promise<void> {
    return this.productsService.sendEmail(user, product, productId);
  }
}
