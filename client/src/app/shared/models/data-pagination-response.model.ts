import { SortType } from '../../../app/products/enums/sort-type.enum';
import { SortBy } from '../../../app/products/enums/sort-by.enum';
import { UserSortBy } from '../../../app/admin/enums/user-sort-by.enum';

export interface PaginationInfo {
  resultsPerPage: number;
  page: number;
  totalResults: number;
}

export interface DataPaginationResponse<T> extends PaginationInfo {
  data: T;
}

export interface PageInfo {
  pageSize: number;
  pageIndex: number;
}

export interface SortInfo {
  sortType: SortType;
  sortBy: SortBy;
}
export interface UserSortInfo {
  sortType: SortType;
  sortBy: UserSortBy;
}
