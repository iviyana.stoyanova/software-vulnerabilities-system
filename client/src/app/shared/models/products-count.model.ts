export interface ProductsCount {
  all: number;
  withCVE: number;
  withoutCVE: number;
}
