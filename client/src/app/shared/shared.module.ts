import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { NgZorroModule } from './ng-zorro.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { FooterComponent } from './components/footer/footer.component';

const allComponents = [HeaderComponent, LoginComponent, FooterComponent];

@NgModule({
  declarations: [...allComponents],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, NgZorroModule],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    NgZorroModule,
    ...allComponents,
  ],
})
export class SharedModule {}
