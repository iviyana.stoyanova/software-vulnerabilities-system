import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormGroupDirective,
} from '@angular/forms';
import { AuthService } from '../../../../app/core/services/auth.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { UsernameValidators } from './username.validators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  public validateForm: FormGroup;

  @Input()
  public isVisible = false;

  @Output()
  public visibilityChange = new EventEmitter<boolean>();

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private notification: NzNotificationService
  ) {}

  ngOnInit(): void {
    this.validateForm = this.formBuilder.group({
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(16),
          UsernameValidators.cannotContainSpace,
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(32),
        ],
      ],
    });
  }

  public submitForm(form: FormGroupDirective): void {
    if (form.valid) {
      this.authService.login(form.value).subscribe(
        (res) => {
          this.notification.success('Successfully sign in!', 'Welcome to SVS', {
            nzPlacement: 'bottomRight',
          });
          this.handleCancel();
        },
        (err) => {
          this.notification.error(
            'Failed sign in!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
      form.resetForm();
    }
  }

  public handleCancel(): void {
    this.isVisible = false;
    this.visibilityChange.emit(this.isVisible);
  }
}
