import { AbstractControl, ValidationErrors } from '@angular/forms';

export class UsernameValidators {
  static cannotContainSpace(control: AbstractControl): ValidationErrors | null {
    if (control && control.value && (control.value as string).includes(' ')) {
      return { cannotContainSpace: true };
    }

    return null;
  }
}
