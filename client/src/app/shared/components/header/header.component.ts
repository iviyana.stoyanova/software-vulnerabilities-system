import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
} from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UserDTO } from '../../../../app/users/models/user.dto';
import { AuthService } from '../../../../app/core/services/auth.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnDestroy {
  public loggedUserData: UserDTO;
  private loggedUserSubscription: Subscription;

  public userAvatar: string;
  public avatarsUrl: string = 'http://localhost:3000/avatars/';

  @Output()
  public loginButtonClicked: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private router: Router,
    private authService: AuthService,
    private notification: NzNotificationService
  ) {}

  public ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => {
        this.loggedUserData = data;
        if (this.loggedUserData && this.loggedUserData.avatarUrl) {
          this.userAvatar = this.avatarsUrl + data.avatarUrl;
        }
      }
    );
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public onClickLoginButton() {
    this.loginButtonClicked.emit(true);
  }

  public navigateTo(path: string) {
    this.router.navigate([path]);
  }

  public logout(): void {
    this.authService.logout().subscribe(
      () =>
        this.notification.success(
          'Successfully sign out!',
          'Welcome back to SVS!',
          {
            nzPlacement: 'bottomRight',
          }
        ),
      (err) =>
        this.notification.error(
          'Failed logout!',
          err.error && err.error.message,
          { nzPlacement: 'bottomRight' }
        )
    );
    this.router.navigate(['home']);
  }

  public navigateToMyProfile() {
    this.router.navigate(['users', this.loggedUserData.username]);
  }
}
