import { NgModule } from '@angular/core';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzModalModule } from 'ng-zorro-antd/modal';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzNotificationModule } from 'ng-zorro-antd/notification';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzUploadModule } from 'ng-zorro-antd/upload';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzDividerModule } from 'ng-zorro-antd/divider';
import { NzResultModule } from 'ng-zorro-antd/result';


const ngZorroModules = [
  NzButtonModule,
  NzModalModule,
  NzInputModule,
  NzIconModule,
  NzFormModule,
  NzSelectModule,
  NzTableModule,
  NzNotificationModule,
  NzSpinModule,
  NzBadgeModule,
  NzCardModule,
  NzUploadModule,
  NzAvatarModule,
  NzPopconfirmModule,
  NzDescriptionsModule,
  NzDividerModule,
  NzResultModule
];

@NgModule({
  declarations: [],
  imports: [...ngZorroModules],
  exports: [...ngZorroModules],
})
export class NgZorroModule {}
