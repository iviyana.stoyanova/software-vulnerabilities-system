import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserDTO } from '../../models/user.dto';
import { UserService } from '../../../../app/core/services/users.service';
import { Subscription } from 'rxjs';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AuthService } from '../../../../app/core/services/auth.service';
import { PasswordValidators } from '../password.validators';
import { UploadChangeParam } from 'ng-zorro-antd/upload';
import { CONFIG } from '../../../../app/config/config';
import { NzMessageService } from 'ng-zorro-antd/message';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit, OnDestroy {
  public validateNameForm: FormGroup;
  public validatePasswordForm: FormGroup;
  public user: UserDTO;
  public loggedUserData: UserDTO;
  private loggedUserSubscription: Subscription;
  public uploadUrl: string;
  public userAvatar: string;
  public avatarsUrl: string = 'http://localhost:3000/avatars/';

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly userService: UserService,
    private readonly authService: AuthService,
    private readonly notification: NzNotificationService,
    private readonly message: NzMessageService
  ) {}

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => {
        this.loggedUserData = data;
        if (data.avatarUrl) {
          this.userAvatar = this.avatarsUrl + data.avatarUrl;
        }
        this.uploadUrl = `${CONFIG.API_DOMAIN_NAME}/users/${this.loggedUserData.username}/avatar`;
      }
    );

    this.validateNameForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(35),
        ],
      ],
    });

    this.validatePasswordForm = this.formBuilder.group({
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(32),
          Validators.pattern(
            /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,32}$/
          ),
        ],
      ],
      confirmPassword: [
        '',
        [Validators.required, PasswordValidators.confirmPassword],
      ],
    });
  }

  public ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public getUser(username: string): void {
    this.userService.getUser(username).subscribe((user) => {
      this.user = user;
    });
  }

  public submitNameForm(form): void {
    if (form.valid) {
      this.userService
        .updateUser(this.loggedUserData.username, form.value)
        .subscribe(
          (res) => {
            this.loggedUserData.name = res.name;
            this.notification.success('Successfully updated name!', '', {
              nzPlacement: 'bottomRight',
            });
          },
          (err) => {
            this.notification.error(
              'Updated name failed!',
              err.error && err.error.message,
              { nzPlacement: 'bottomRight' }
            );
          }
        );
      form.resetForm();
    }
  }

  public submitPasswordForm(form): void {
    if (form.valid) {
      this.userService
        .updateUserPassword(this.loggedUserData.username, form.value)
        .subscribe(
          (res) => {
            this.notification.success('Successfully updated password!', '', {
              nzPlacement: 'bottomRight',
            });
          },
          (err) => {
            this.notification.error(
              'Updated password failed!',
              err.error && err.error.message,
              { nzPlacement: 'bottomRight' }
            );
          }
        );
      form.resetForm();
    }
  }

  public uploadAvatar(info: UploadChangeParam): void {
    if (info.file.status === 'error') {
      this.message.error(`${info.file.name} file upload failed!`);
    } else if (info.file.status === 'done') {
      this.message.success(`${info.file.name} file upload success!`);
      this.userAvatar = this.avatarsUrl + info.file.response.avatarUrl;
      this.loggedUserData.avatarUrl = info.file.response.avatarUrl;
      this.authService.emitUserData(this.loggedUserData);
    }
  }
}
