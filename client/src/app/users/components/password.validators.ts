import { ValidationErrors, FormControl } from '@angular/forms';

export class PasswordValidators {
  static confirmPassword(
    confirmPassword: FormControl
  ): ValidationErrors | null {
    if (!confirmPassword.parent) {
      return null;
    }
    const password = confirmPassword.parent.controls['password'];
    return password && password.value !== confirmPassword.value
      ? { passwordsNotTheSame: true }
      : null;
  }
}
