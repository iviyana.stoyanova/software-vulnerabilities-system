import { UserRole } from './enums/user-role.enum';

export class UserDTO {
  id: string;
  username: string;
  email: string;
  name: string;
  avatarUrl?: string;
  role?: UserRole;
}
