import { UserRole } from './enums/user-role.enum';
export class UpdateUserDTO {
  public avatarUrl?: string;
  public name?: string;
  public username?: string;
  public email?: string;
  public password?: string;
  public role?: UserRole;
}
