import { NgZorroModule } from './../../../shared/ng-zorro.module';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { AdminService } from '../../../../app/core/services/admin.service';
import {
  ReactiveFormsModule,
  FormBuilder,
  FormGroupDirective,
  FormGroup,
  AbstractControl,
} from '@angular/forms';
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { CreateUserComponent } from './create-user.component';
import { SharedModule } from '../../../../app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { RegistrationCredentialsDTO } from './models/registration-credentials.dto';
import { UserRole } from '../../../../app/users/models/enums/user-role.enum';
import { of, throwError } from 'rxjs';
import { mock } from 'jest-mock-extended';

describe('CreateUserComponent', () => {
  let formBuilder: any;
  let adminService: any;
  let notification: any;

  let component: CreateUserComponent;
  let fixture: ComponentFixture<CreateUserComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    formBuilder = {
      group() {},
    };
    adminService = {
      register() {},
    };

    notification = {
      success() {},
      error() {},
    };

    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        ReactiveFormsModule,
        RouterTestingModule,
        NgZorroModule,
      ],
      declarations: [CreateUserComponent],
      providers: [AdminService, FormBuilder, NzNotificationService],
    })
      .overrideProvider(AdminService, { useValue: adminService })
      .overrideProvider(FormBuilder, { useValue: formBuilder })
      .overrideProvider(NzNotificationService, { useValue: notification })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(CreateUserComponent);
        component = fixture.componentInstance;
      });
  }));

  it('should create', () => {
    // Arrange & Act & Assert
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should call formBuilder.group() once with correct parameters', () => {
      // Arrange

      let formConfig: any;
      const spy = jest
        .spyOn(formBuilder, 'group')
        .mockImplementation((config) => (formConfig = config));
      // Act
      component.ngOnInit();
      // Assert
      expect(spy).toHaveBeenCalledTimes(1);
      expect(formConfig.name).toBeDefined();
      expect(formConfig.username).toBeDefined();
      expect(formConfig.email).toBeDefined();
      expect(formConfig.password).toBeDefined();
      expect(formConfig.confirmPassword).toBeDefined();
      expect(formConfig.role).toBeDefined();
    });
  });

  describe('submitForm()', () => {
    it('should call adminService.register() once with correct parameters', () => {
      // Arrange
      const mockedUserCredentials: RegistrationCredentialsDTO = {
        name: 'Iviyana Stoyanova',
        username: 'iviyana',
        email: 'iviyana@test.com',
        password: '12345p$',
        role: UserRole.BASIC,
      };
      const form = mock<FormGroupDirective>();
      Object.defineProperty(form, 'value', {
        get: () => mockedUserCredentials,
      });
      Object.defineProperty(form, 'valid', { get: () => true });
      const registerSpy = jest
        .spyOn(adminService, 'register')
        .mockReturnValue(of('mocked register return value'));
      // Act
      component.submitForm(form);
      // Assert
      expect(registerSpy).toHaveBeenCalledTimes(1);
      expect(registerSpy).toBeCalledWith(mockedUserCredentials);
    });
    it('should not call adminService.register() when the form is not valid', () => {
      // Arrange
      const form = mock<FormGroupDirective>();
      Object.defineProperty(form, 'valid', { get: () => false, set: () => {} });
      const registerSpy = jest.spyOn(adminService, 'register');
      // Act
      component.submitForm(form);
      // Assert
      expect(registerSpy).toHaveBeenCalledTimes(0);
    });
    it('should call notification.success() once with correct message when the register is successful', () => {
      // Arrange
      const mockedUserCredentials: RegistrationCredentialsDTO = {
        name: 'Iviyana Stoyanova',
        username: 'iviyana',
        email: 'iviyana@test.com',
        password: '12345p$',
        role: UserRole.BASIC,
      };
      const form = mock<FormGroupDirective>();
      Object.defineProperty(form, 'value', {
        get: () => mockedUserCredentials,
      });
      Object.defineProperty(form, 'valid', { get: () => true });
      jest
        .spyOn(adminService, 'register')
        .mockReturnValue(of('mocked register return value'));

      const successSpy = jest.spyOn(notification, 'success');
      // Act
      component.submitForm(form);
      // Assert
      expect(successSpy).toHaveBeenCalledTimes(1);
      expect(successSpy).toBeCalledWith('Successfully added new user!', '', {
        nzPlacement: 'bottomRight',
      });
    });
    it('should call notification.error() once with correct message when the register failed', () => {
      // Arrange
      const mockedUserCredentials: RegistrationCredentialsDTO = {
        name: 'Iviyana Stoyanova',
        username: 'iviyana',
        email: 'iviyana@test.com',
        password: '12345p$',
        role: UserRole.BASIC,
      };
      const form = mock<FormGroupDirective>();
      Object.defineProperty(form, 'value', {
        get: () => mockedUserCredentials,
      });
      Object.defineProperty(form, 'valid', { get: () => true });
      jest
        .spyOn(adminService, 'register')
        .mockReturnValue(
          throwError({ error: { message: 'Mocked registration failed' } })
        );

      const errorSpy = jest.spyOn(notification, 'error');
      // Act
      component.submitForm(form);
      // Assert
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toBeCalledWith(
        'Adding new user failed!',
        'Mocked registration failed',
        { nzPlacement: 'bottomRight' }
      );
    });
  });
  describe('nzValidationStatus()', () => {
    it('should return error when the form control is touched and invalid', () => {
      // Arrange
      const form = mock<FormGroup>();
      const control = mock<AbstractControl>();
      Object.defineProperty(control, 'touched', { get: () => true });
      Object.defineProperty(control, 'invalid', { get: () => true });
      form.controls = { testName: control };
      component.validateForm = form;
      // Act
      const res: string = component.nzValidationStatus('testName');
      // Assert
      expect(res).toEqual('error');
    });
    it('should return success when the form control is touched and valid', () => {
      // Arrange
      const form = mock<FormGroup>();
      const control = mock<AbstractControl>();
      Object.defineProperty(control, 'touched', { get: () => true });
      Object.defineProperty(control, 'invalid', {
        get: () => false,
        set: () => {},
      });
      form.controls = { testName: control };
      component.validateForm = form;
      // Act
      const res: string = component.nzValidationStatus('testName');
      // Assert
      expect(res).toEqual('success');
    });
    it('should return empty string when the form control is not touched', () => {
      // Arrange
      const form = mock<FormGroup>();
      const control = mock<AbstractControl>();
      Object.defineProperty(control, 'touched', {
        get: () => false,
        set: () => {},
      });
      form.controls = { testName: control };
      component.validateForm = form;
      // Act
      const res: string = component.nzValidationStatus('testName');
      // Assert
      expect(res).toEqual('');
    });
  });
});
