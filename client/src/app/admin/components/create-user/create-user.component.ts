import { AdminService } from './../../../core/services/admin.service';
import { UserRole } from './../../../users/models/enums/user-role.enum';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormGroupDirective, ValidatorFn, ValidationErrors, FormControl } from '@angular/forms';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { RegistrationCredentialsDTO } from './models/registration-credentials.dto';
import { UsernameValidators } from '../../../../app/shared/components/login/username.validators';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {
  public validateForm: FormGroup;

  @Input()
  public isVisible = false;


  @Output()
  public onUserCreated: EventEmitter<void> = new EventEmitter();

  public roles: UserRole[] = [UserRole.BASIC, UserRole.ADMIN];

  public passwordValidator: ValidatorFn = (confirmPassword: FormControl): ValidationErrors | null => {
    if (!confirmPassword.parent) {
      return null;
    }
    const password = confirmPassword.parent.controls['password'];
    return password && password.value !== confirmPassword.value ? { passwordsNotTheSame: true } : null;
  }
  constructor(
    private formBuilder: FormBuilder,
    private adminService: AdminService,
    private notification: NzNotificationService
  ) {}

  ngOnInit(): void {
    this.validateForm = this.formBuilder.group({
      name: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(35),
        ],
      ],
      username: [
        '',
        [
          Validators.required,
          Validators.minLength(3),
          Validators.maxLength(16),
          UsernameValidators.cannotContainSpace,
        ],
      ],
      email: [
        '',
        [
          Validators.required,
          Validators.maxLength(60),
          Validators.pattern(/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
        ],
      ],
      password: [
        '',
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(32),
          Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{5,32}$/)
        ],
      ],
      confirmPassword: [
        '',
        [this.passwordValidator],
      ],
      role: [
        UserRole.BASIC,
        [
          Validators.required,
        ],
      ],
    });
  }

  public submitForm(form: FormGroupDirective): void {
    if (form.valid) {
      const createUserRequest: RegistrationCredentialsDTO = {
        ...form.value
      };
      delete createUserRequest['confirmPassword'];

      this.adminService.register(createUserRequest).subscribe(
        (res) => {
          this.notification.success('Successfully added new user!', '',
            { nzPlacement: 'bottomRight' }
          );
          this.isVisible = false;
          this.onUserCreated.emit();
        },
        (err) => {
          this.notification.error(
            'Adding new user failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
      form.resetForm();
    }
  }

  public nzValidationStatus(controlName: string): string {
    return this.validateForm.controls[controlName].touched ?
      (this.validateForm.controls[controlName].invalid ?
        'error' : 'success')
      : '';
  }
}
