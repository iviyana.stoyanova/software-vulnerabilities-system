import { UserRole } from '../../../../users/models/enums/user-role.enum';
export class RegistrationCredentialsDTO {
  name: string;
  username: string;
  password: string;
  email: string;
  role: UserRole;
}
