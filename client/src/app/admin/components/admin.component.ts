import { UserSortInfo, PageInfo } from './../../shared/models/data-pagination-response.model';
import { UserDTO } from './../../users/models/user.dto';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { AdminService } from '../../../app/core/services/admin.service';
import { ActivatedRoute } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { DataPaginationResponse, PaginationInfo } from '../../../app/shared/models/data-pagination-response.model';
import { UserSortBy } from '../enums/user-sort-by.enum';
import { SortType } from '../../../app/products/enums/sort-type.enum';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  public isVisibleRegisterModal = false;
  public users: UserDTO[] = [];
  public paginationInfo: PaginationInfo;
  public cpeLastModifiedDate: Date;
  private pageInfo: PageInfo = { pageSize: 10, pageIndex: 1 };
  private sortInfo: UserSortInfo = { sortType: SortType.ASC, sortBy: UserSortBy.NAME };

  constructor(
    private cd: ChangeDetectorRef,
    private readonly notificationService: NzNotificationService,
    private readonly adminService: AdminService,
    private readonly activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      (data: { usersResolver: DataPaginationResponse<UserDTO[]> }) => {
        this.users = data.usersResolver.data;
        this.paginationInfo = {
          page: data.usersResolver.page,
          resultsPerPage: data.usersResolver.resultsPerPage,
          totalResults: data.usersResolver.totalResults
        };
      }
    );
    this.getLastDateOfModification();
  }

  public showRegisterModal() {
    this.isVisibleRegisterModal = false;
    this.cd.detectChanges();
    this.isVisibleRegisterModal = true;
  }

  public allUsers(): void {
    this.adminService
      .getAllUsers(this.pageInfo.pageIndex, this.pageInfo.pageSize, this.sortInfo.sortBy, this.sortInfo.sortType)
      .subscribe(
        (response) => {
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults
          };
          this.users = response.data;
        },
        (err) => {
          this.notificationService.error(
            'Showing all users failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public usersSort(sortInfo: UserSortInfo): void {
    this.sortInfo = sortInfo;
    this.allUsers();
  }

  public usersPagination(pageInfo: PageInfo): void {
    this.pageInfo = pageInfo;
    this.allUsers();
  }

  public updateCPEs(): void {
    this.adminService.updateCPEsRepository()
      .subscribe(
        () => {
          this.notificationService.success(
            'CPE repository update started in the background!',
            '',
            { nzPlacement: 'bottomRight' }
          );
        },
        (err) => {
          this.notificationService.error(
            'CPE Repository update could not start!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public getLastDateOfModification(): void {
    this.adminService.getLastDateOfModification()
      .subscribe(
        (data) => {
          this.cpeLastModifiedDate = data;
        },
        () => {
          this.cpeLastModifiedDate = null;
        }
      );
  }
}
