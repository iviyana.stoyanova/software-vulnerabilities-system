import {
  PageInfo,
  UserSortInfo,
} from './../../../shared/models/data-pagination-response.model';
import { UserDTO } from './../../../users/models/user.dto';
import { AuthService } from './../../../core/services/auth.service';
import { UserService } from './../../../core/services/users.service';
import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { SharedModule } from '../../../../app/shared/shared.module';
import { NgZorroModule } from '../../../../app/shared/ng-zorro.module';
import { UsersTableComponent } from './users-table.component';
import { RouterTestingModule } from '@angular/router/testing';
import { AdminService } from '../../../../app/core/services/admin.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { UserRole } from '../../../../app/users/models/enums/user-role.enum';
import { of, throwError } from 'rxjs';
import { cpus } from 'os';
import { SortType } from '../../../../app/products/enums/sort-type.enum';
import { UserSortBy } from '../../enums/user-sort-by.enum';
import { mock } from 'jest-mock-extended';
import { NzTableQueryParams } from 'ng-zorro-antd/table/public-api';
import { UpdateUserDTO } from '../../../../app/users/models/update-user.dto';

describe('UsersTableComponent', () => {
  let userService: any;
  let adminService: any;
  let authService: any;
  let notificationService: any;

  let component: UsersTableComponent;
  let fixture: ComponentFixture<UsersTableComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    userService = {
      updateUser() {},
    };

    adminService = {
      deleteUser() {},
    };

    authService = {
      get loggedUserData$() {
        return null;
      },
    };

    notificationService = {
      success() {},
      error() {},
    };

    TestBed.configureTestingModule({
      imports: [SharedModule, NgZorroModule, RouterTestingModule],
      declarations: [UsersTableComponent],
      providers: [
        AdminService,
        UserService,
        AuthService,
        NzNotificationService,
      ],
    })
      .overrideProvider(UserService, { useValue: userService })
      .overrideProvider(AdminService, { useValue: adminService })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(NzNotificationService, {
        useValue: notificationService,
      })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(UsersTableComponent);
        component = fixture.componentInstance;
      });
  }));
  it('should create', () => {
    // Arrange & Act & Assert
    expect(component).toBeTruthy();
  });

  describe('ngOnInit()', () => {
    it('should call authService.loggedUserData once', () => {
      // Arrange
      const mockUser: UserDTO = {
        id: '1',
        name: 'Iviyana Stoyanova',
        username: 'iviyana',
        email: 'iviyana@test.com',
        role: UserRole.BASIC,
      };

      const loggedUserDataSpy = jest
        .spyOn(authService, 'loggedUserData$', 'get')
        .mockReturnValue(of(mockUser));
      // Act
      component.ngOnInit();
      // Assert
      expect(loggedUserDataSpy).toHaveBeenCalledTimes(1);
      expect(component.loggedUserData).toEqual(mockUser);
    });
  });

  describe('ngOnDestroy()', () => {
    it('should unsubscribe loggedUserSubscription', () => {
      // Arrange
      const subscription = of(null).subscribe();
      const subSpy = jest.spyOn(subscription, 'unsubscribe');
      // tslint:disable-next-line: no-string-literal
      component['loggedUserSubscription'] = subscription;
      // Act
      component.ngOnDestroy();
      // Assert
      expect(subSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('onPageNumberChange()', () => {
    it('should call onPageClicked.emit() once with the correct parameter', () => {
      // Arrange
      const pageInfo: PageInfo = {
        pageSize: 10,
        pageIndex: 5,
      };
      component.pagination = {
        resultsPerPage: 10,
        page: 2,
        totalResults: 20,
      };
      const emitSpy = jest.spyOn(component.onPageClicked, 'emit');
      // Act
      component.onPageNumberChange(5);
      // Assert
      expect(emitSpy).toHaveBeenCalledTimes(1);
      expect(emitSpy).toHaveBeenCalledWith(pageInfo);
    });
  });
  describe('onPageSizeChange()', () => {
    it('should call onPageClicked.emit() once with the correct parameter', () => {
      // Arrange
      const pageInfo: PageInfo = {
        pageSize: 20,
        pageIndex: 2,
      };
      component.pagination = {
        resultsPerPage: 20,
        page: 2,
        totalResults: 20,
      };
      const emitSpy = jest.spyOn(component.onPageClicked, 'emit');
      // Act
      component.onPageSizeChange(20);
      // Assert
      expect(emitSpy).toHaveBeenCalledTimes(1);
      expect(emitSpy).toHaveBeenCalledWith(pageInfo);
    });
  });
  describe('onQueryParamsChange()', () => {
    it('should call onSortClicked.emit() once with the correct parameter', () => {
      // Arrange
      const sortInfo: UserSortInfo = {
        sortType: SortType.ASC,
        sortBy: UserSortBy.NAME,
      };

      const emitSpy = jest.spyOn(component.onSortClicked, 'emit');
      const nzTableQueryParams = mock<NzTableQueryParams>();
      nzTableQueryParams.sort = [{ key: 'name', value: 'ascend' }];
      // Act
      component.onQueryParamsChange(nzTableQueryParams);
      // Assert
      expect(emitSpy).toHaveBeenCalledTimes(1);
      expect(emitSpy).toHaveBeenCalledWith(sortInfo);
    });
    it('should not call onSortClicked.emit() when the sort is not changed', () => {
      // Arrange
      const emitSpy = jest.spyOn(component.onSortClicked, 'emit');
      const nzTableQueryParams = mock<NzTableQueryParams>();
      nzTableQueryParams.sort = [{ key: 'name', value: 'ascend' }];
      component.currentSortChange = { key: 'name', value: 'ascend' };
      // Act
      component.onQueryParamsChange(nzTableQueryParams);
      // Assert
      expect(emitSpy).toHaveBeenCalledTimes(0);
    });
  });
  describe('updateUser()', () => {
    it('should notify the correct error when the username is empty!', () => {
      // Arrange
      component.usernameInput = '';
      component.nameInput = 'test';
      component.emailInput = 'test@gmail.com';

      const errorSpy = jest.spyOn(notificationService, 'error');

      const updateSpy = jest.spyOn(userService, 'updateUser');
      // Act
      component.updateUser();
      // Assert
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toHaveBeenCalledWith(
        'Username should not be empty!',
        '',
        { nzPlacement: 'bottomRight' }
      );
      expect(updateSpy).toHaveBeenCalledTimes(0);
    });
    it('should notify the correct error when the name is empty!', () => {
      // Arrange
      component.usernameInput = 'test';
      component.nameInput = '';
      component.emailInput = 'test@gmail.com';

      const errorSpy = jest.spyOn(notificationService, 'error');

      const updateSpy = jest.spyOn(userService, 'updateUser');
      // Act
      component.updateUser();
      // Assert
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toHaveBeenCalledWith('Name should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
      expect(updateSpy).toHaveBeenCalledTimes(0);
    });
    it('should notify the correct error when the email is empty!', () => {
      // Arrange
      component.usernameInput = 'test';
      component.nameInput = 'Test';
      component.emailInput = '';

      const errorSpy = jest.spyOn(notificationService, 'error');

      const updateSpy = jest.spyOn(userService, 'updateUser');
      // Act
      component.updateUser();
      // Assert
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toHaveBeenCalledWith('Email should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
      expect(updateSpy).toHaveBeenCalledTimes(0);
    });
    it('should handle correctly a successful user update', () => {
      // Arrange
      const mockUpdatedUser: UserDTO = {
        id: '1',
        username: 'test',
        name: 'Test',
        role: UserRole.BASIC,
        email: 'test@gmail.com',
        avatarUrl: null,
      };
      component.usernameInput = 'test';
      component.nameInput = 'Test';
      component.emailInput = 'test@gmail.com';
      component.roleInput = UserRole.BASIC;

      const updatedUser: UpdateUserDTO = {
        username: 'test',
        name: 'Test',
        email: 'test@gmail.com',
        role: UserRole.BASIC,
        avatarUrl: null,
      };

      component.selectedUser = {
        id: '1',
        username: 'selected',
        name: 'Selected',
        role: UserRole.ADMIN,
        email: 'selected@gmail.com',
        avatarUrl: null,
      };

      component.users = [component.selectedUser];
      component.showEditMode = true;

      const updateSpy = jest
        .spyOn(userService, 'updateUser')
        .mockReturnValue(of(mockUpdatedUser));

      const successSpy = jest.spyOn(notificationService, 'success');
      // Act
      component.updateUser();
      // Assert
      expect(updateSpy).toHaveBeenCalledTimes(1);
      expect(updateSpy).toHaveBeenCalledWith('selected', updatedUser);
      expect(successSpy).toHaveBeenCalledWith(
        "Successfully updated selected's details!",
        '',
        {
          nzPlacement: 'bottomRight',
        }
      );
      expect(component.users).toHaveLength(1);
      expect(component.users).toContain(mockUpdatedUser);
      expect(component.showEditMode).toBeFalsy();
    });
    it('should notify the correct error when the updateUser method fails', () => {
      // Arrange
      component.usernameInput = 'test';
      component.nameInput = 'Test';
      component.emailInput = 'test@gmail.com';
      component.roleInput = UserRole.BASIC;

      const updatedUser: UpdateUserDTO = {
        username: 'test',
        name: 'Test',
        email: 'test@gmail.com',
        role: UserRole.BASIC,
        avatarUrl: null,
      };

      component.selectedUser = {
        id: '1',
        username: 'selected',
        name: 'Selected',
        role: UserRole.ADMIN,
        email: 'selected@gmail.com',
        avatarUrl: null,
      };

      const updateSpy = jest
        .spyOn(userService, 'updateUser')
        .mockReturnValue(
          throwError({ error: { message: 'Update user failed!' } })
        );

      const errorSpy = jest.spyOn(notificationService, 'error');
      // Act
      component.updateUser();
      // Assert
      expect(updateSpy).toHaveBeenCalledTimes(1);
      expect(updateSpy).toHaveBeenCalledWith('selected', updatedUser);
      expect(
        errorSpy
      ).toHaveBeenCalledWith(
        "Failed to update selected's details!",
        'Update user failed!',
        { nzPlacement: 'bottomRight' }
      );
    });
  });

  describe('onEditButtonClicked()', () => {
    it("should set the selected user, assign the user's properties to the respective inputs and activate the edit mode", () => {
      // Arrange
      const mockedUser: UserDTO = {
        id: '1',
        username: 'mocked',
        name: 'Mocked',
        role: UserRole.ADMIN,
        email: 'mocked@gmail.com',
      };
      component.selectedUser = null;
      component.showEditMode = false;
      // Act
      component.onEditButtonClicked(mockedUser);
      // Assert
      expect(component.selectedUser).toBe(mockedUser);
      expect(component.nameInput).toBe(mockedUser.name);
      expect(component.usernameInput).toBe(mockedUser.username);
      expect(component.emailInput).toBe(mockedUser.email);
      expect(component.roleInput).toBe(mockedUser.role);
      expect(component.showEditMode).toBeTruthy();
    });
  });

  describe('deleteUser()', () => {
    it('should handle correctly a successful user delete', () => {
      // Arrange
      const mockDeletedUser: UserDTO = {
        id: '1',
        username: 'mocked',
        name: 'Mocked',
        role: UserRole.ADMIN,
        email: 'mocked@gmail.com',
      };
      component.users = [mockDeletedUser];

      const deleteSpy = jest
        .spyOn(adminService, 'deleteUser')
        .mockReturnValue(of(null));

      const successSpy = jest.spyOn(notificationService, 'success');
      // Act
      component.deleteUser(mockDeletedUser);
      // Assert
      expect(deleteSpy).toHaveBeenCalledTimes(1);
      expect(deleteSpy).toHaveBeenCalledWith('mocked');
      expect(successSpy).toHaveBeenCalledWith(
        'Succesfully deleted user Mocked!',
        '',
        {
          nzPlacement: 'bottomRight',
        }
      );
      expect(component.users).toHaveLength(0);
    });
    it('should notify the correct error when the deleteUser method fails', () => {
      // Arrange
      const mockDeletedUser: UserDTO = {
        id: '1',
        username: 'mocked',
        name: 'Mocked',
        role: UserRole.ADMIN,
        email: 'mocked@gmail.com',
      };
      component.users = [mockDeletedUser];

      const deleteSpy = jest
        .spyOn(adminService, 'deleteUser')
        .mockReturnValue(
          throwError({ error: { message: 'Delete user request failed!' } })
        );

      const errorSpy = jest.spyOn(notificationService, 'error');
      // Act
      component.deleteUser(mockDeletedUser);
      // Assert
      expect(deleteSpy).toHaveBeenCalledTimes(1);
      expect(deleteSpy).toHaveBeenCalledWith('mocked');
      expect(errorSpy).toHaveBeenCalledWith(
        'Delete user failed!',
        'Delete user request failed!',
        {
          nzPlacement: 'bottomRight',
        }
      );
      expect(component.users).toHaveLength(1);
    });
  });
});
