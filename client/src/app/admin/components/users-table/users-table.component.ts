import { AdminService } from '../../../../app/core/services/admin.service';
import { UserService } from '../../../../app/core/services/users.service';
import { UpdateUserDTO } from '../../../../app/users/models/update-user.dto';
import { UserRole } from './../../../users/models/enums/user-role.enum';
import { UserSortBy } from './../../enums/user-sort-by.enum';
import { SortType } from './../../../products/enums/sort-type.enum';
import {
  PaginationInfo,
  PageInfo,
  UserSortInfo,
} from './../../../shared/models/data-pagination-response.model';
import { UserDTO } from './../../../users/models/user.dto';
import {
  Component,
  OnInit,
  EventEmitter,
  Input,
  Output,
  OnDestroy,
} from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { NzTableQueryParams } from 'ng-zorro-antd/table/src/table.types';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../../app/core/services/auth.service';

@Component({
  selector: 'app-users-table',
  templateUrl: './users-table.component.html',
  styleUrls: ['./users-table.component.scss'],
})
export class UsersTableComponent implements OnInit, OnDestroy {
  public currentSortChange: { key: string; value: string } = {
    key: null,
    value: null,
  };
  public avatarsUrl: string = 'http://localhost:3000/avatars/';

  @Input()
  public users: UserDTO[];
  @Input()
  public pagination: PaginationInfo;

  @Output()
  public onPageClicked: EventEmitter<PageInfo> = new EventEmitter();
  @Output()
  public onSortClicked: EventEmitter<UserSortInfo> = new EventEmitter();

  public showEditMode = false;
  public nameInput: string;
  public usernameInput: string;
  public emailInput: string;
  public roleInput: UserRole;
  public roles: UserRole[] = [UserRole.BASIC, UserRole.ADMIN];
  public selectedUser: UserDTO;
  public loggedUserData: UserDTO;
  public avatarInput: string;
  private loggedUserSubscription: Subscription;

  constructor(
    private readonly notificationService: NzNotificationService,
    private readonly userService: UserService,
    private readonly adminService: AdminService,
    private authService: AuthService
  ) {}

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => {
        this.loggedUserData = data;
      }
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription?.unsubscribe();
  }

  public onPageNumberChange(page: number): void {
    const params: PageInfo = {
      pageSize: this.pagination.resultsPerPage,
      pageIndex: page,
    };
    this.onPageClicked.emit(params);
  }

  public onPageSizeChange(size: number): void {
    const params: PageInfo = {
      pageSize: size,
      pageIndex: this.pagination.page,
    };
    this.onPageClicked.emit(params);
  }

  public onQueryParamsChange(params: NzTableQueryParams): void {
    const { sort } = params;
    const sortChange = sort.find((el) => el.value);
    if (
      sortChange &&
      (sortChange.value !== this.currentSortChange.value ||
        sortChange.key !== this.currentSortChange.key)
    ) {
      this.currentSortChange = sortChange;
      const sortInfo: UserSortInfo = {
        sortType: sortChange.value === 'ascend' ? SortType.ASC : SortType.DESC,
        sortBy: sortChange.key as UserSortBy,
      };
      this.onSortClicked.emit(sortInfo);
    }
  }

  public updateUser(): void {
    let validInput = true;
    if (!this.usernameInput.trim()) {
      validInput = false;
      this.notificationService.error('Username should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
    }
    if (!this.nameInput.trim()) {
      validInput = false;
      this.notificationService.error('Name should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
    }
    if (!this.emailInput.trim()) {
      validInput = false;
      this.notificationService.error('Email should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
    }
    if (!validInput) {
      return;
    }

    const updatedUser: UpdateUserDTO = {
      username: this.usernameInput,
      name: this.nameInput,
      email: this.emailInput,
      role: this.roleInput,
      avatarUrl: this.avatarInput || null,
    };
    this.userService
      .updateUser(this.selectedUser.username, updatedUser)
      .subscribe(
        (data) => {
          this.notificationService.success(
            `Successfully updated ${this.selectedUser.username}'s details!`,
            '',
            {
              nzPlacement: 'bottomRight',
            }
          );
          this.users.splice(this.users.indexOf(this.selectedUser), 1, data);
          this.showEditMode = false;
        },
        (err) => {
          this.notificationService.error(
            `Failed to update ${this.selectedUser.username}'s details!`,
            err.error && err.error.message,
            {
              nzPlacement: 'bottomRight',
            }
          );
        }
      );
  }

  public onEditButtonClicked(user: UserDTO): void {
    this.selectedUser = user;
    this.nameInput = user.name;
    this.usernameInput = user.username;
    this.emailInput = user.email;
    this.roleInput = user.role;
    this.showEditMode = true;
    this.avatarInput = user.avatarUrl;
  }

  public deleteUser(user: UserDTO): void {
    this.adminService.deleteUser(user?.username).subscribe(
      () => {
        this.notificationService.success(
          `Succesfully deleted user ${user.name}!`,
          '',
          {
            nzPlacement: 'bottomRight',
          }
        );
        this.users.splice(this.users.indexOf(user), 1);
      },
      (err) => {
        this.notificationService.error(
          'Delete user failed!',
          err.error && err.error.message,
          { nzPlacement: 'bottomRight' }
        );
      }
    );
  }
}
