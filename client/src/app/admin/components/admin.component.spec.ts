import { PaginationInfo, UserSortInfo, PageInfo } from './../../shared/models/data-pagination-response.model';
import { UsersResolverService } from './../resolvers/users.resolver';
import { UsersTableComponent } from './users-table/users-table.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { AdminComponent } from './admin.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { NgZorroModule } from 'src/app/shared/ng-zorro.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AdminService } from 'src/app/core/services/admin.service';
import { ActivatedRoute } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ChangeDetectorRef } from '@angular/core';
import { of, throwError } from 'rxjs';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { JwtModule, JwtHelperService } from '@auth0/angular-jwt';
import { UserDTO } from 'src/app/users/models/user.dto';
import { DataPaginationResponse } from 'src/app/shared/models/data-pagination-response.model';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UserSortBy } from '../enums/user-sort-by.enum';
import { SortType } from 'src/app/products/enums/sort-type.enum';
import { subscribeOn } from 'rxjs/operators';
describe('AdminComponent', () => {
  let adminService: any;
  let activatedRoute: any;
  let notificationService: any;
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    adminService = {
      getAllUsers() {
        return {
          subscribe() {}
        };
      },
      updateCPEsRepository() {},
      getLastDateOfModification() {},
    };

    activatedRoute = {
      data: null
    };

    notificationService = {
      success() {},
      error() {},
    };

    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        NgZorroModule,
        RouterTestingModule,
        HttpClientTestingModule,
        JwtModule.forRoot({ config: {} }),
        BrowserAnimationsModule
      ],
      declarations: [AdminComponent, CreateUserComponent, UsersTableComponent],
      providers: [AdminService, NzNotificationService, ChangeDetectorRef],
    })
      .overrideProvider(AdminService, { useValue: adminService })
      .overrideProvider(NzNotificationService, {
        useValue: notificationService,
      })
      .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(AdminComponent);
        component = fixture.componentInstance;
      });
  }));
  it('should create', () => {
    // Arrange & Act & Assert
    expect(component).toBeTruthy();
  });
  describe('ngOnInit()', () => {
    it('should initialize users from activatedRoute.data and call getLastDateOfModification()', () => {
      // Arrange
      const users: UserDTO[] = [
        {
          id: '1',
          username: 'test',
          name: 'Test',
          email: 'test@gmail.com'
        }
      ];
      const paginationInfo: PaginationInfo = {
        page: 3,
        resultsPerPage: 10,
        totalResults: 42
      };
      const paginationResponse: DataPaginationResponse<UserDTO[]> = {
        data: users,
        resultsPerPage: 10,
        page: 3,
        totalResults: 42
      };
      activatedRoute.data = of({ usersResolver: paginationResponse });
      const lastModifiedSpy = jest
        .spyOn(component, 'getLastDateOfModification')
        .mockImplementation(() => {});
      // Act
      component.ngOnInit();
      // Assert
      expect(component.users).toEqual(users);
      expect(component.paginationInfo).toEqual(paginationInfo);
      expect(lastModifiedSpy).toHaveBeenCalledTimes(1);
    });
  });
  describe('showRegisterModal()', () => {
    it('should call cd.detectChanges() and update isVisibleRegisterModal accordingly', () => {
      // Arrange
      let intermediateVisibleModal = true;
      const cdSpy = jest
        .spyOn((component as any).cd, 'detectChanges')
        .mockImplementation(() => {
          intermediateVisibleModal = component.isVisibleRegisterModal;
        });
      // Act
      component.showRegisterModal();
      // Assert
      expect(cdSpy).toHaveBeenCalledTimes(1);
      expect(intermediateVisibleModal).toBeFalsy();
      expect(component.isVisibleRegisterModal).toBeTruthy();
    });
  });
  describe('allUsers()', () => {
    it('should call adminService.getAllUsers once with the correct parameters', () => {
      // Arrange
      (component as any).pageInfo = {
        pageIndex: 3,
        pageSize: 20
      };
      (component as any).sortInfo = {
        sortBy: UserSortBy.NAME,
        sortType: SortType.DESC
      };
      const getAllUsersSpy = jest
        .spyOn(adminService, 'getAllUsers');

      // Act
      component.allUsers();
      // Assert
      expect(getAllUsersSpy).toHaveBeenCalledTimes(1);
      expect(getAllUsersSpy).toHaveBeenCalledWith(3, 20, UserSortBy.NAME, SortType.DESC);
    });
    it('should refresh users after a successful request', () => {
      // Arrange
      const users: UserDTO[] = [
        {
          id: '1',
          username: 'test',
          name: 'Test',
          email: 'test@gmail.com'
        }
      ];
      const paginationInfo: PaginationInfo = {
        page: 3,
        resultsPerPage: 10,
        totalResults: 42
      };
      const paginationResponse: DataPaginationResponse<UserDTO[]> = {
        data: users,
        resultsPerPage: 10,
        page: 3,
        totalResults: 42
      };

      jest
        .spyOn(adminService, 'getAllUsers')
        .mockReturnValue(of(paginationResponse));

      // Act
      component.allUsers();
      // Assert
      expect(component.paginationInfo).toEqual(paginationInfo);
      expect(component.users).toEqual(users);
    });
    it('should notify the error after an unsuccessful request', () => {
      // Arrange
      jest
        .spyOn(adminService, 'getAllUsers')
        .mockReturnValue(throwError({ error: { message: 'Request to backend failed!' } }));

      const errorSpy = jest.spyOn(notificationService, 'error');
      // Act
      component.allUsers();
      // Assert
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toBeCalledWith(
        'Showing all users failed!',
        'Request to backend failed!',
        { nzPlacement: 'bottomRight' }
      );
    });
  });
  describe('usersSort()', () => {
    it('should update sortInfo and call allUsers', () => {
      // Arrange
      const sortInfo: UserSortInfo = {
        sortBy: UserSortBy.USERNAME,
        sortType: SortType.DESC
      };
      const allUsersSpy = jest
        .spyOn(component, 'allUsers')
        .mockImplementation(() => {});
      // Act
      component.usersSort(sortInfo);
      // Assert
      const componentSortInfo = (component as any).sortInfo;
      expect(componentSortInfo).toEqual(sortInfo);
      expect(allUsersSpy).toHaveBeenCalledTimes(1);
    });
  });
  describe('usersPagination()', () => {
    it('should update pageInfo and call allUsers', () => {
      // Arrange
      const pageInfo: PageInfo = {
        pageSize: 25,
        pageIndex: 15
      };
      const allUsersSpy = jest
        .spyOn(component, 'allUsers')
        .mockImplementation(() => {});
      // Act
      component.usersPagination(pageInfo);
      // Assert
      const componentPageInfo = (component as any).pageInfo;
      expect(componentPageInfo).toEqual(pageInfo);
      expect(allUsersSpy).toHaveBeenCalledTimes(1);
    });
  });
  describe('updateCPEs()', () => {
    it('should call adminService.updateCPEsRepository once and notify user when the request was successful', () => {
      // Arrange
      const updateCPEsSpy = jest
        .spyOn(adminService, 'updateCPEsRepository')
        .mockReturnValue(of(null));
      const successSpy = jest.spyOn(notificationService, 'success');
      // Act
      component.updateCPEs();
      // Assert
      expect(updateCPEsSpy).toHaveBeenCalledTimes(1);
      expect(successSpy).toHaveBeenCalledTimes(1);
      expect(successSpy).toBeCalledWith(
        'CPE repository update started in the background!',
        '',
        { nzPlacement: 'bottomRight' }
      );
    });
    it('should call adminService.updateCPEsRepository once and notify user when the request was unsuccessful', () => {
      // Arrange
      const updateCPEsSpy = jest
        .spyOn(adminService, 'updateCPEsRepository')
        .mockReturnValue(throwError({ error: { message: 'Error starting CPE updater!' } }));
      const errorSpy = jest.spyOn(notificationService, 'error');
      // Act
      component.updateCPEs();
      // Assert
      expect(updateCPEsSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toHaveBeenCalledTimes(1);
      expect(errorSpy).toBeCalledWith(
        'CPE Repository update could not start!',
        'Error starting CPE updater!',
        { nzPlacement: 'bottomRight' }
      );
    });
  });
  describe('getLastDateOfModification()', () => {
    it('should call adminService.getLastDateOfModification once and update the date when the request was successful', () => {
      // Arrange
      const date = new Date(1023);
      const getLastDateOfModificationSpy = jest
        .spyOn(adminService, 'getLastDateOfModification')
        .mockReturnValue(of(date));
      // Act
      component.getLastDateOfModification();
      // Assert
      expect(getLastDateOfModificationSpy).toHaveBeenCalledTimes(1);
      expect(component.cpeLastModifiedDate).toEqual(date);
    });
    it('should call adminService.getLastDateOfModification once and set the date to null when the request was unsuccessful', () => {
      // Arrange
      component.cpeLastModifiedDate = new Date(2000);
      const getLastDateOfModificationSpy = jest
        .spyOn(adminService, 'getLastDateOfModification')
        .mockReturnValue(throwError(''));
      // Act
      component.getLastDateOfModification();
      // Assert
      expect(getLastDateOfModificationSpy).toHaveBeenCalledTimes(1);
      expect(component.cpeLastModifiedDate).toBeNull();
    });
  });
});
