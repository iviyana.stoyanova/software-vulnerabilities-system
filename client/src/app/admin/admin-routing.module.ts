import { AdminGuard } from './../core/guards/admin.guard';
import { UsersResolverService } from './resolvers/users.resolver';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './components/admin.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../core/guards/auth.guard';

const routes: Routes = [
  { path: '',
    component: AdminComponent,
    resolve: { usersResolver: UsersResolverService },
    canActivate: [AuthGuard, AdminGuard]
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class AdminRoutingModule {}
