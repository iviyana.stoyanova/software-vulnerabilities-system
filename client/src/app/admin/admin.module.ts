import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from './../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateUserComponent } from './components/create-user/create-user.component';
import { AdminComponent } from './components/admin.component';
import { UsersTableComponent } from './components/users-table/users-table.component';
import { UsersResolverService } from './resolvers/users.resolver';

@NgModule({
  declarations: [
    CreateUserComponent,
    AdminComponent,
    UsersTableComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    AdminRoutingModule
  ],
  providers: [
    UsersResolverService
  ],

  exports: [
    AdminComponent,
  ]
})
export class AdminModule { }
