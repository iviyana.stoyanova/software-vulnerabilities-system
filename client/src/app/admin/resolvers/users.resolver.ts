import { UserSortBy } from './../enums/user-sort-by.enum';
import { UserDTO } from './../../users/models/user.dto';
import { AdminService } from './../../core/services/admin.service';
import { Injectable } from '@angular/core';
import { Resolve } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, timeout, tap } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataPaginationResponse } from '../../../app/shared/models/data-pagination-response.model';
import { SortType } from '../../../app/products/enums/sort-type.enum';

@Injectable()
export class UsersResolverService
  implements Resolve<DataPaginationResponse<UserDTO[]>> {
  constructor(
    private readonly adminService: AdminService,
    private readonly notificationService: NzNotificationService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(
  ): Observable<DataPaginationResponse<UserDTO[]>> {
    this.spinner.show();
    return this.adminService
      .getAllUsers(1, 10, UserSortBy.NAME, SortType.ASC)
      .pipe(
        tap(() => {
          this.spinner.hide();
        }),
        timeout(5000),
        catchError((err) => {
          this.spinner.hide();
          this.notificationService.error(
            'Failed! The Users list is not accessible!',
            err.error && err.error.message,
            { nzPlacement: 'topRight' }
          );
          const emptyData: DataPaginationResponse<UserDTO[]> = {
            resultsPerPage: null,
            page: null,
            totalResults: null,
            data: [],
          };

          return of(emptyData);
        })
      );
  }
}
