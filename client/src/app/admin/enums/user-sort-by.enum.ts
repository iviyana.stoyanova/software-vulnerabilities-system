export enum UserSortBy {
  NAME = 'name',
  USERNAME = 'username'
}
