import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CveTableComponent } from './components/cve-table/cve-table.component';
import { HomeComponent } from './components/home/home.component';
import { NgZorroModule } from '../shared/ng-zorro.module';
import { CVEResolverService } from './resolvers/cve.resolver';
import { HomeRoutingModule } from './home-routing.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [HomeComponent, CveTableComponent],
  imports: [CommonModule, FormsModule, NgZorroModule, HomeRoutingModule],
  providers: [CVEResolverService],
})
export class HomeModule {}
