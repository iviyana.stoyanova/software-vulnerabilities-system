import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { HomeComponent } from './components/home/home.component';
import { CVEResolverService } from './resolvers/cve.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: { cveResolver: CVEResolverService },
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeRoutingModule {}
