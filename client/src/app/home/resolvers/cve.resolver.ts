import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, timeout, tap } from 'rxjs/operators';
import { CVEService } from '../../../app/core/services/cves.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { DataPaginationResponse } from '../../../app/shared/models/data-pagination-response.model';
import { CVE } from '../models/cve.model';

@Injectable()
export class CVEResolverService
  implements Resolve<DataPaginationResponse<CVE[]>> {
  constructor(
    private readonly cvesService: CVEService,
    private readonly notificationService: NzNotificationService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot
  ): Observable<DataPaginationResponse<CVE[]>> {
    this.spinner.show();
    return this.cvesService.getCves('1', '10').pipe(
      tap(() => {
        this.spinner.hide();
      }),
      timeout(10000),
      catchError((err) => {
        this.spinner.hide();
        this.notificationService.error(
          'Failed! The NVD Data Feeds is not accessible!',
          err.error && err.error.message,
          { nzPlacement: 'topRight' }
        );
        const emptyData: DataPaginationResponse<CVE[]> = {
          resultsPerPage: null,
          page: null,
          totalResults: null,
          data: [],
        };

        return of(emptyData);
      })
    );
  }
}
