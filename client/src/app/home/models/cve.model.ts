export interface CVE {
  cveId: string;
  description: string;
  cpeUri?: string;
  versionEndIncluding?: number;
  vulnerable?: string;
  lastModifiedDate: string;
  severity?: string;
}
