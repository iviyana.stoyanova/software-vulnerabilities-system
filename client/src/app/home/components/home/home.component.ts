import { Component, OnInit } from '@angular/core';
import { CVEService } from '../../../../app/core/services/cves.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ActivatedRoute } from '@angular/router';
import { NzTableQueryParams } from 'ng-zorro-antd/table/src/table.types';
import {
  PaginationInfo,
  DataPaginationResponse,
} from '../../../../app/shared/models/data-pagination-response.model';
import { CVE } from '../../models/cve.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  public cves: CVE[];
  public paginationInfo: PaginationInfo;
  public loadingPage: boolean;

  constructor(
    private readonly cvesService: CVEService,
    private readonly notificationService: NzNotificationService,
    private readonly activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      (data: { cveResolver: DataPaginationResponse<CVE[]> }) => {
        this.cves = data.cveResolver.data;
        this.paginationInfo = {
          page: data.cveResolver.page,
          resultsPerPage: data.cveResolver.resultsPerPage,
          totalResults: data.cveResolver.totalResults
            ? data.cveResolver.totalResults
            : 0,
        };
      }
    );
  }

  public cvesDataFromServer(params: Partial<NzTableQueryParams>): void {
    const { pageSize, pageIndex } = params;
    this.loadingPage = true;
    this.cvesService
      .getCves(pageIndex.toString(), pageSize.toString())
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.cves = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public cveDataFromServer(cveId: string): void {
    this.loadingPage = true;
    if (!cveId) {
      this.cvesDataFromServer({ pageIndex: 1, pageSize: 10 });
    } else {
      this.cvesService.getCve(cveId).subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.cves = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Invalid CVE-ID!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
    }
  }
}
