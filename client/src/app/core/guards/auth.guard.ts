import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NzNotificationService } from 'ng-zorro-antd/notification';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router,
    private readonly notificationService: NzNotificationService
  ) {}

  public canActivate(): boolean {
    if (!this.authService.getUserDataIfAuthenticated()) {
      this.notificationService.error(
        'Failed',
        'You must be logged in in order to see this page!',
        { nzPlacement: 'bottomRight' }
      );
      this.router.navigate(['home']);

      return false;
    }

    return true;
  }
}
