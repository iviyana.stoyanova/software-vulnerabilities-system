import { UserDTO } from './../../users/models/user.dto';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { UserRole } from '../../../app/users/models/enums/user-role.enum';
import { StorageService } from '../services/storage.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class AdminGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly notificationService: NzNotificationService,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService
  ) {}

  public canActivate(): boolean {
    const token = this.storage.getItem('token');
    const userData: UserDTO = this.jwtService.decodeToken(token);
    if (userData.role !== UserRole.ADMIN) {
      this.notificationService.error(
        'Failed',
        'You must have admin rights in order to see this page!',
        { nzPlacement: 'bottomRight' }
      );
      this.router.navigate(['home']);

      return false;
    }

    return true;
  }
}
