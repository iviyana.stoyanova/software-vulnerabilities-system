import { TestBed, async } from '@angular/core/testing';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CONFIG } from '../../../app/config/config';
import { of } from 'rxjs';
import { ProductMatchingService } from './product.matching.service';
import { Product } from '../../../app/products/models/product.model';
import { UpdateProduct } from 'src/app/products/models/update-product.model';

describe('ProductMatchingService', () => {
  let httpClient;

  let service: ProductMatchingService;

  beforeEach(async(() => {
    jest.clearAllMocks();

    httpClient = {
      get() {},
      post() {},
      put() {},
      delete() {},
    };

    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ProductMatchingService],
    }).overrideProvider(HttpClient, { useValue: httpClient });
    /* tslint:disable:deprecation*/
    service = TestBed.get(ProductMatchingService);
  }));

  it('should be defined', () => {
    // Arrange & Act & Assert
    expect(service).toBeDefined();
  });

  describe('getMatchingCPEs() should', () => {
    it('call the httpClient.get() method once', (done) => {
      // Arrange
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/matchingCPEs`;
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getMatchingCPEs(productId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('return the result from the httpClient.get() method', () => {
      // Arrange
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/matchingCPEs`;
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act
      const result = service.getMatchingCPEs(productId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('assignCPE() should', () => {
    it('call the httpClient.put() method once', (done) => {
      // Arrange
      const cpeUri = 'cpe:2.3:a:act:compass:-:*:*:*:*:*:*:*';
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cpe/${cpeUri}`;
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act & Assert
      service.assignCPE(productId, cpeUri).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, {});

        done();
      });
    });

    it('return the result from the httpClient.put() method', () => {
      // Arrange
      const cpeUri = 'cpe:2.3:a:act:compass:-:*:*:*:*:*:*:*';
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cpe/${cpeUri}`;
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act
      const result = service.assignCPE(productId, cpeUri);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('assignCVE() should', () => {
    it('call the httpClient.put() method once', (done) => {
      // Arrange
      const cveId = 'CVE-2020-6831';
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cves/${cveId}`;
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act & Assert
      service.assignCVE(productId, cveId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, {});

        done();
      });
    });

    it('return the result from the httpClient.put() method', () => {
      // Arrange
      const cveId = 'CVE-2020-6831';
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cves/${cveId}`;
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act
      const result = service.assignCVE(productId, cveId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('unassignCVE() should', () => {
    it('call the httpClient.put() method once', (done) => {
      // Arrange
      const cveId = 'CVE-2020-6831';
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cves/${cveId}`;
      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

      // Act & Assert
      service.unassignCVE(productId, cveId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('return the result from the httpClient.delete() method', () => {
      // Arrange
      const cveId = 'CVE-2020-6831';
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cves/${cveId}`;
      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

      // Act
      const result = service.unassignCVE(productId, cveId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('getAllCVEs() should', () => {
    it('call the httpClient.get() method once', (done) => {
      // Arrange
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cves`;
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act & Assert
      service.getAllCVEs(productId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('return the result from the httpClient.get() method', () => {
      // Arrange
      const productId = 1;
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/cves`;
      const spy = jest.spyOn(httpClient, 'get').mockReturnValue(returnValue);

      // Act
      const result = service.getAllCVEs(productId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('sendEmail() should', () => {
    it('call the httpClient.post() method once', (done) => {
      // Arrange
      const productId = 1;
      const mockProduct: Product = {
        id: 1,
        product: 'Firefox',
        vendor: 'Mozilla',
        version: '72.0.2',
        addedOn: '2020-06-05 22:09:09',
      };
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/email`;
      const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

      // Act & Assert
      service.sendEmail(mockProduct, productId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, mockProduct);

        done();
      });
    });

    it('return the result from the httpClient.post() method', () => {
      // Arrange
      const productId = 1;
      const mockProduct: Product = {
        id: 1,
        product: 'Firefox',
        vendor: 'Mozilla',
        version: '72.0.2',
        addedOn: '2020-06-05 22:09:09',
      };
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}/email`;
      const spy = jest.spyOn(httpClient, 'post').mockReturnValue(returnValue);

      // Act
      const result = service.sendEmail(mockProduct, productId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('updateProduct() should', () => {
    it('call the httpClient.put() method once', (done) => {
      // Arrange
      const productId = 1;
      const mockProduct: UpdateProduct = new UpdateProduct();
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}`;
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act & Assert
      service.updateProduct(productId, mockProduct).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url, mockProduct);

        done();
      });
    });

    it('return the result from the httpClient.put() method', () => {
      // Arrange
      const productId = 1;
      const mockProduct: Product = {
        id: 1,
        product: 'Firefox',
        vendor: 'Mozilla',
        version: '72.0.2',
        addedOn: '2020-06-05 22:09:09',
      };
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}`;
      const spy = jest.spyOn(httpClient, 'put').mockReturnValue(returnValue);

      // Act
      const result = service.updateProduct(productId, mockProduct);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });

  describe('deleteProduct() should', () => {
    it('call the httpClient.delete() method once', (done) => {
      // Arrange
      const productId = 1;
      const mockProduct: UpdateProduct = new UpdateProduct();
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}`;
      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

      // Act & Assert
      service.deleteProduct(productId).subscribe(() => {
        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(url);

        done();
      });
    });

    it('return the result from the httpClient.delete() method', () => {
      // Arrange
      const productId = 1;
      const mockProduct: Product = {
        id: 1,
        product: 'Firefox',
        vendor: 'Mozilla',
        version: '72.0.2',
        addedOn: '2020-06-05 22:09:09',
      };
      const returnValue = of('return value');
      const baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;
      const url = `${baseUrl}/${productId}`;
      const spy = jest.spyOn(httpClient, 'delete').mockReturnValue(returnValue);

      // Act
      const result = service.deleteProduct(productId);

      // Assert
      expect(result).toEqual(returnValue);
    });
  });
});
