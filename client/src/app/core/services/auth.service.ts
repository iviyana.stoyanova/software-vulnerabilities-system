import { CONFIG } from './../../config/config';
import { RegistrationCredentialsDTO } from '../../admin/components/create-user/models/registration-credentials.dto';
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { JwtHelperService } from '@auth0/angular-jwt';
import { UserDTO } from '../../../app/users/models/user.dto';
import { UserCredentialsDTO } from '../../../app/users/models/user-credentials.dto';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private loggedUserDataSubject$ = new BehaviorSubject<UserDTO>(
    this.getUserDataIfAuthenticated()
  );

  public constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
    private readonly jwtService: JwtHelperService
  ) {}

  public get loggedUserData$(): Observable<UserDTO> {
    return this.loggedUserDataSubject$.asObservable();
  }

  public emitUserData(user: UserDTO): void {
    this.loggedUserDataSubject$.next(user);
  }

  public getUserDataIfAuthenticated(): UserDTO {
    const token: string = this.storage.getItem('token');

    if (token && this.jwtService.isTokenExpired(token)) {
      this.storage.removeItem('token');
      return null;
    }

    return token ? this.jwtService.decodeToken(token) : null;
  }

  public login(user: UserCredentialsDTO): Observable<any> {
    return this.http
      .post<UserCredentialsDTO>(`${CONFIG.API_DOMAIN_NAME}/session`, user)
      .pipe(
        tap(({ token }) => {
          this.storage.setItem('token', token);
          const userData: UserDTO = this.jwtService.decodeToken(token);
          this.emitUserData(userData);
        })
      );
  }

  public logout(): Observable<any> {
    return this.http.delete(`${CONFIG.API_DOMAIN_NAME}/session`).pipe(
      tap(() => {
        this.storage.removeItem('token');
        this.emitUserData(null);
      })
    );
  }
}
