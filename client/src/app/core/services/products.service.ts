import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../../app/config/config';
import { SortType } from '../../../app/products/enums/sort-type.enum';
import { SortBy } from '../../../app/products/enums/sort-by.enum';
import { Product } from '../../../app/products/models/product.model';
import { DataPaginationResponse } from '../../../app/shared/models/data-pagination-response.model';
import { ProductsCount } from '../../../app/shared/models/products-count.model';
import { CreateProduct } from '../../../app/products/models/create-product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductsService {
  private baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;

  constructor(private readonly http: HttpClient) {}

  public getAllProducts(
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<Product[]>> {
    const url = `${this.baseUrl}?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}`;
    return this.http.get<DataPaginationResponse<Product[]>>(url);
  }

  public getProductsCount(): Observable<ProductsCount> {
    const url = `${this.baseUrl}/number-of-products`;
    return this.http.get<ProductsCount>(url);
  }

  public getProductsWithCVEs(
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<Product[]>> {
    const url = `${this.baseUrl}/cves?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}`;
    return this.http.get<DataPaginationResponse<Product[]>>(url);
  }

  public getProductsWithoutCVEs(
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<Product[]>> {
    const url = `${this.baseUrl}/without-cves?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}`;
    return this.http.get<DataPaginationResponse<Product[]>>(url);
  }

  public searchByVendor(
    vendor: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<Product[]>> {
    const url = `${this.baseUrl}/search/byVendor?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}&searchString=${vendor}`;
    return this.http.get<DataPaginationResponse<Product[]>>(url);
  }

  public searchByProduct(
    product: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<Product[]>> {
    const url = `${this.baseUrl}/search/byProduct?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}&searchString=${product}`;
    return this.http.get<DataPaginationResponse<Product[]>>(url);
  }

  public searchByCPE(
    cpe: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<Product[]>> {
    const url = `${this.baseUrl}/search/byCPE?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}&searchString=${cpe}`;
    return this.http.get<DataPaginationResponse<Product[]>>(url);
  }

  public searchByCVE(
    cve: string,
    page: number,
    resultsPerPage: number,
    sortBy: SortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<Product[]>> {
    const url = `${this.baseUrl}/search/byCVE?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}&searchString=${cve}`;
    return this.http.get<DataPaginationResponse<Product[]>>(url);
  }

  public addProduct(body: CreateProduct): Observable<Product> {
    const url = this.baseUrl;
    return this.http.post<Product>(url, body);
  }

  public getProduct(productId: number): Observable<Product> {
    const url = `${this.baseUrl}/${productId}`;
    return this.http.get<Product>(url);
  }
}
