import { UpdateProduct } from './../../products/models/update-product.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CPE } from '../../../app/products/models/cpe.model';
import { CONFIG } from '../../../app/config/config';
import { Product } from '../../../app/products/models/product.model';
import { CVEsForProduct } from '../../../app/products/models/cves-for-product.model';
import { DeleteProduct } from '../../../app/products/models/delete-product.model';

@Injectable({
  providedIn: 'root',
})
export class ProductMatchingService {
  public baseUrl: string = `${CONFIG.API_DOMAIN_NAME}/products`;

  constructor(private readonly http: HttpClient) {}

  public getMatchingCPEs(productId: number): Observable<CPE[]> {
    const url = `${this.baseUrl}/${productId}/matchingCPEs`;
    return this.http.get<CPE[]>(url);
  }

  public assignCPE(productId: number, cpeUri: string): Observable<Product> {
    const url = `${this.baseUrl}/${productId}/cpe/${cpeUri}`;
    return this.http.put<Product>(url, {});
  }

  public assignCVE(productId: number, cveId: string): Observable<Product> {
    const url = `${this.baseUrl}/${productId}/cves/${cveId}`;
    return this.http.put<Product>(url, {});
  }

  public unassignCVE(productId: number, cveId: string): Observable<void> {
    const url = `${this.baseUrl}/${productId}/cves/${cveId}`;
    return this.http.delete<void>(url);
  }

  public getAllCVEs(productId: number): Observable<CVEsForProduct> {
    const url = `${this.baseUrl}/${productId}/cves`;
    return this.http.get<CVEsForProduct>(url);
  }

  public sendEmail(body: Product, productId: number): Observable<void> {
    const url = `${this.baseUrl}/${productId}/email`;
    return this.http.post<void>(url, body);
  }

  public updateProduct(
    productId: number,
    body: UpdateProduct
  ): Observable<Product> {
    const url = `${this.baseUrl}/${productId}`;
    return this.http.put<Product>(url, body);
  }

  public deleteProduct(productId: number): Observable<DeleteProduct> {
    const url = `${this.baseUrl}/${productId}`;
    return this.http.delete<DeleteProduct>(url);
  }
}
