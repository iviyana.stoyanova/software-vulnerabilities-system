import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../../app/config/config';
import { DataPaginationResponse } from '../../../app/shared/models/data-pagination-response.model';
import { CVE } from '../../../app/home/models/cve.model';

@Injectable({
  providedIn: 'root',
})
export class CVEService {
  constructor(private readonly http: HttpClient) {}

  public getCve(cveId: string): Observable<DataPaginationResponse<CVE[]>> {
    const url = `${CONFIG.API_DOMAIN_NAME}/cves/${cveId}`;
    return this.http.get<DataPaginationResponse<CVE[]>>(url);
  }

  public getCves(
    page: string,
    perPage: string
  ): Observable<DataPaginationResponse<CVE[]>> {
    const url = `${CONFIG.API_DOMAIN_NAME}/cves?page=${page}&perPage=${perPage}`;
    return this.http.get<DataPaginationResponse<CVE[]>>(url);
  }
}
