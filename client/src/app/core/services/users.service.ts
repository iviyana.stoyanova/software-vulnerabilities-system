import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CONFIG } from '../../../app/config/config';
import { UserDTO } from '../../../app/users/models/user.dto';
import { UpdateUserDTO } from '../../../app/users/models/update-user.dto';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private readonly http: HttpClient) {}

  public getUser(username: string): Observable<UserDTO> {
    const url = `${CONFIG.API_DOMAIN_NAME}/users/${username}`;
    return this.http.get<UserDTO>(url);
  }

  public updateUser(
    username: string,
    body: UpdateUserDTO
  ): Observable<UserDTO> {
    const url = `${CONFIG.API_DOMAIN_NAME}/users/${username}`;
    return this.http.put<UserDTO>(url, body);
  }

  public updateUserPassword(
    username: string,
    body: UpdateUserDTO
  ): Observable<UserDTO> {
    const url = `${CONFIG.API_DOMAIN_NAME}/users/${username}/password`;
    return this.http.put<UserDTO>(url, body);
  }
}
