import { UserSortBy } from './../../admin/enums/user-sort-by.enum';
import { UserDTO } from './../../users/models/user.dto';
import { DataPaginationResponse } from './../../shared/models/data-pagination-response.model';
import { SortType } from './../../products/enums/sort-type.enum';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RegistrationCredentialsDTO } from '../../../app/admin/components/create-user/models/registration-credentials.dto';
import { Observable } from 'rxjs';
import { CONFIG } from '../../../app/config/config';
import { DeleteUser } from '../../../app/admin/models/delete-user.model';

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  constructor(
    private readonly http: HttpClient
  ) { }

  public register(
    user: RegistrationCredentialsDTO
  ): Observable<RegistrationCredentialsDTO> {
    return this.http.post<RegistrationCredentialsDTO>(
      `${CONFIG.API_DOMAIN_NAME}/users`,
      user
    );
  }

  public getAllUsers(
    page: number,
    resultsPerPage: number,
    sortBy: UserSortBy,
    sortType: SortType
  ): Observable<DataPaginationResponse<UserDTO[]>> {
    const url = `${CONFIG.API_DOMAIN_NAME}/users?page=${page}&resultsPerPage=${resultsPerPage}&sortBy=${sortBy}&sortType=${sortType}`;
    return this.http.get<DataPaginationResponse<UserDTO[]>>(url);
  }

  public updateCPEsRepository(): Observable<void> {
    const url = `${CONFIG.API_DOMAIN_NAME}/cpe`;
    return this.http.put<void>(url, {});
  }

  public getLastDateOfModification(): Observable<Date> {
    const url = `${CONFIG.API_DOMAIN_NAME}/cpe/lastModifiedDate`;
    return this.http.get<Date>(url);
  }

  public deleteUser(username: string): Observable<DeleteUser> {
    const url = `${CONFIG.API_DOMAIN_NAME}/users/${username}`;
    return this.http.delete<DeleteUser>(url);
  }
}
