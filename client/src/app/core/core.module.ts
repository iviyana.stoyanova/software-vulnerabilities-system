import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';

@NgModule({
  providers: [
    AuthService,
    StorageService
  ],
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  exports: [ HttpClientModule ],
})
export class CoreModule {}
