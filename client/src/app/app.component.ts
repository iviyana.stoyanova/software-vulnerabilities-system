import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'client';
  isVisibleLoginModal = false;

  constructor() {}
  showLoginModal(isVisible: boolean) {
    this.isVisibleLoginModal = isVisible;
  }
}
