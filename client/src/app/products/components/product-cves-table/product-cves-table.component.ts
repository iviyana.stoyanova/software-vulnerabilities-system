import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CVE } from '../../../../app/home/models/cve.model';
import { CVEsTableView } from '../../enums/cves-table-view.enum';

@Component({
  selector: 'app-product-cves-table',
  templateUrl: './product-cves-table.component.html',
  styleUrls: ['./product-cves-table.component.scss']
})
export class ProductCVEsTableComponent implements OnInit {
  @Input()
  public productCVEs: CVE[];

  @Input()
  public loading: boolean;

  @Input()
  public tableView: CVEsTableView;

  @Output()
  public assignCVE: EventEmitter<CVE> = new EventEmitter();

  @Output()
  public unassignCVE: EventEmitter<CVE> = new EventEmitter();

  constructor() { }

  public sortSeverity = (a: CVE, b: CVE) => {
    if (a.severity === b.severity) {
      return 0;
    }
    if (a.severity === 'HIGH') {
      return 1;
    }
    if (a.severity === 'LOW') {
      return -1;
    }
    if (b.severity === 'LOW') {
      return 1;
    }
    return -1;
  }
  public sortDate = (a: CVE, b: CVE) => a.lastModifiedDate.localeCompare(b.lastModifiedDate);
  public sortCVEId = (a: CVE, b: CVE) => a.cveId.localeCompare(b.cveId);

  ngOnInit(): void {
  }

  public onAssignClicked(cve: CVE): void {
    this.assignCVE.emit(cve);
  }

  public onUnassignClicked(cve: CVE): void {
    this.unassignCVE.emit(cve);
  }
}
