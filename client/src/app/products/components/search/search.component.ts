import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
  public radioButtonOption = 'vendor';
  public searchString: string;

  @Output()
  public onSearchClicked: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {}

  public onSearchButtonClicked(): void {
    this.onSearchClicked.emit({
      searchString: this.searchString,
      radioButtonOption: this.radioButtonOption
    });
  }
}
