import { UpdateProduct } from './../../models/update-product.model';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { CPE } from '../../models/cpe.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductMatchingService } from '../../../../app/core/services/product.matching.service';
import { Product } from '../../models/product.model';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { CVE } from '../../../../app/home/models/cve.model';
import { CVEsTableView } from '../../enums/cves-table-view.enum';
import { UserDTO } from '../../../../app/users/models/user.dto';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../../app/core/services/auth.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss'],
})
export class ProductDetailsComponent implements OnInit, OnDestroy {
  public cpes: CPE[] = [];
  public product: Product;
  public showCPETable: boolean;
  public loadingCPEs: boolean;
  public cves: CVE[] = [];
  public loadingCVEs: boolean;
  public assignedCVEs: CVE[] = [];
  public unassignedCVEs: CVE[] = [];
  public cvesTableViews = CVEsTableView;
  public cvesTableView: CVEsTableView = CVEsTableView.unassigned;
  public showEditMode = false;
  public productInput: string;
  public vendorInput: string;
  public versionInput: string;
  public showNotifyAdminButton: boolean = false;
  public loggedUserData: UserDTO;
  public isUserAdmin: boolean;
  public loadingSendEmail: boolean = false;
  private loggedUserSubscription: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly productsMatchingService: ProductMatchingService,
    private readonly notificationService: NzNotificationService,
    private router: Router,
    private authService: AuthService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.loggedUserSubscription = this.authService.loggedUserData$.subscribe(
      (data: UserDTO) => {
        this.loggedUserData = data;
        if (this.loggedUserData) {
          this.loggedUserData?.role === 'Admin'
            ? (this.isUserAdmin = true)
            : (this.isUserAdmin = false);
        }
      }
    );

    this.activatedRoute.data.subscribe(
      (data: { productDetailsResolver: Product }) => {
        this.product = data.productDetailsResolver;
        if (this.product && this.product.cpe) {
          this.showCPETable = false;
          this.getCVEs();
        } else {
          this.showCPETable = true;
          this.getMatchingCPEs(this.product.id);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.loggedUserSubscription.unsubscribe();
  }

  public assignCPEtoProduct(cpe: CPE): void {
    this.spinner.show();
    this.productsMatchingService
      .assignCPE(this.product.id, cpe.cpeUri)
      .subscribe(
        (product) => {
          this.spinner.hide();
          this.showCPETable = false;
          this.product.cpe = product.cpe;
          this.notificationService.success(
            'Succesfuly assigned CPE to Product!',
            ''
          );
          this.getCVEs();
        },
        (err) => {
          this.spinner.hide();
          this.notificationService.error(
            'CPE assignment failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  private getMatchingCPEs(productId: number): void {
    this.loadingCPEs = true;
    this.productsMatchingService.getMatchingCPEs(productId).subscribe(
      (cpes) => {
        this.loadingCPEs = false;
        this.cpes = cpes;
      },
      (err) => {
        this.loadingCPEs = false;
        this.notificationService.error(
          'CPE fetching failed!',
          err.error && err.error.message,
          { nzPlacement: 'bottomRight' }
        );
      }
    );
  }

  public getCVEs(): void {
    this.loadingCVEs = true;
    this.productsMatchingService.getAllCVEs(this.product.id).subscribe(
      (data) => {
        this.assignedCVEs = data.assignedCVEs;
        this.unassignedCVEs = data.unassignedCVEs;
        if (this.cvesTableView === this.cvesTableViews.assigned) {
          this.cves = this.assignedCVEs;
        } else {
          this.cves = this.unassignedCVEs;
        }
        this.loadingCVEs = false;
      },
      (err) => {
        this.loadingCVEs = false;
        this.notificationService.error(
          'CVE fetching failed!',
          err.error && err.error.message,
          { nzPlacement: 'bottomRight' }
        );
      }
    );
  }

  public assignCVEtoProduct(cve: CVE): void {
    this.spinner.show();
    this.productsMatchingService
      .assignCVE(this.product.id, cve.cveId)
      .subscribe(
        () => {
          this.spinner.hide();
          this.showNotifyAdminButton = true;
          this.notificationService.success(
            'Succesfully assigned CVE to Product!',
            '',
            { nzPlacement: 'bottomRight' }
          );
          this.unassignedCVEs.splice(this.unassignedCVEs.indexOf(cve), 1);
          this.assignedCVEs.unshift(cve);
          this.unassignedCVEs = [].concat(this.unassignedCVEs);
          this.assignedCVEs = [].concat(this.assignedCVEs);
          this.setTableView(this.cvesTableViews.unassigned);
        },
        (err) => {
          this.spinner.hide();
          this.notificationService.error(
            'CVE assignment failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public unassignCVEtoProduct(cve: CVE): void {
    this.productsMatchingService
      .unassignCVE(this.product.id, cve.cveId)
      .subscribe(
        () => {
          this.notificationService.success('Succesfully unassigned CVE!', '', {
            nzPlacement: 'bottomRight',
          });
          this.assignedCVEs.splice(this.assignedCVEs.indexOf(cve), 1);
          this.unassignedCVEs.unshift(cve);
          this.unassignedCVEs = [].concat(this.unassignedCVEs);
          this.assignedCVEs = [].concat(this.assignedCVEs);
          this.setTableView(this.cvesTableViews.assigned);
        },
        (err) => {
          this.notificationService.error(
            'CVE unassignment failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }
  public setTableView(type: CVEsTableView): void {
    this.cvesTableView = type;
    if (type === CVEsTableView.assigned) {
      this.cves = this.assignedCVEs;
    } else {
      this.cves = this.unassignedCVEs;
    }
  }

  public onEditButtonClicked(): void {
    this.productInput = this.product.product;
    this.vendorInput = this.product.vendor;
    this.versionInput = this.product.version;
    this.showEditMode = true;
  }

  public sendEmail(product: Product): void {
    this.loadingSendEmail = true;
    this.productsMatchingService.sendEmail(product, product.id).subscribe(
      (res) => {
        this.loadingSendEmail = false;
        this.showNotifyAdminButton = false;
        this.notificationService.success(
          'Successfully sent an email to System Admin!',
          '',
          {
            nzPlacement: 'bottomRight',
          }
        );
      },
      (err) => {
        this.loadingSendEmail = false;
        this.notificationService.error(
          'Failed to send email to System Admin!',
          '',
          {
            nzPlacement: 'bottomRight',
          }
        );
      }
    );
  }

  public updateProduct(): void {
    let validInput = true;
    if (!this.productInput.trim()) {
      validInput = false;
      this.notificationService.error('Product name should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
    }
    if (!this.vendorInput.trim()) {
      validInput = false;
      this.notificationService.error('Vendor should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
    }
    if (!this.versionInput.trim()) {
      validInput = false;
      this.notificationService.error('Version should not be empty!', '', {
        nzPlacement: 'bottomRight',
      });
    }
    if (!validInput) {
      return;
    }

    const updatedProduct: UpdateProduct = {
      product: this.productInput,
      vendor: this.vendorInput,
      version: this.versionInput,
    };
    this.productsMatchingService
      .updateProduct(this.product.id, updatedProduct)
      .subscribe(
        (data) => {
          this.product = data;
          this.notificationService.success('Succesfully updated product!', '', {
            nzPlacement: 'bottomRight',
          });
          this.getMatchingCPEs(this.product.id);
        },
        (err) => {
          this.notificationService.error(
            'Updating product failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        },
        () => {
          this.showEditMode = false;
        }
      );
  }

  public deleteProduct(): void {
    this.productsMatchingService.deleteProduct(this.product.id).subscribe(
      () => {
        this.notificationService.success('Succesfully deleted product!', '', {
          nzPlacement: 'bottomRight',
        });
        this.router.navigate(['products']);
      },
      (err) => {
        this.notificationService.error(
          'Delete product failed!',
          err.error && err.error.message,
          { nzPlacement: 'bottomRight' }
        );
      }
    );
  }
}
