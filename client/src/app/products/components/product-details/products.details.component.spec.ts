import { ProductDetailsComponent } from './product-details.component';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { SharedModule } from '../../../../app/shared/shared.module';
import { ProductsRoutingModule } from '../../products-routing.module';
import { CommonModule } from '@angular/common';
import { ProductsTableComponent } from '../products-table/products-table.component';
import { ProductsComponent } from '../products/products.component';
import { SearchComponent } from '../search/search.component';
import { ProductCPEsTableComponent } from '../product-cpes-table/product-cpes-table.component';
import { ProductCVEsTableComponent } from '../product-cves-table/product-cves-table.component';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ProductDetailsResolverService } from '../../resolvers/product-details.resolver';
import { ProductsResolverService } from '../../resolvers/products.resolver';
import { ProductMatchingService } from '../../../../app/core/services/product.matching.service';
import { AuthService } from '../../../../app/core/services/auth.service';
import { ActivatedRoute, Router, Routes } from '@angular/router';
import { Product } from '../../models/product.model';
import { of } from 'rxjs';
import { RouterTestingModule } from '@angular/router/testing';
import { UserDTO } from '../../../../app/users/models/user.dto';
import { UserRole } from '../../../../app/users/models/enums/user-role.enum';
import { CPE } from '../../models/cpe.model';

describe('ProductDetailsComponent', () => {
  const routes: Routes = [
    { path: '', component: ProductsComponent },
    { path: ':productId', component: ProductDetailsComponent },
  ];
  const activatedRoute = {
    data: of({ productDetailsResolver: null }),
  };

  let notificationService: any;
  let productMatchingService: any;
  let router: any;
  let authService: any;

  let component: ProductDetailsComponent;
  let fixture: ComponentFixture<ProductDetailsComponent>;

  beforeEach(async(() => {
    jest.clearAllMocks();

    authService = {
      loggedUserData$() {},
    };

    notificationService = {
      success() {},
      error() {},
    };

    router = {
      navigate() {},
    };

    productMatchingService = {
      assignCPE() {},
      assignCVE() {},
      unassignCVE() {},
      getMatchingCPEs() {},
      getAllCVEs() {},
      sendEmail() {},
      updateProduct() {},
      deleteProduct() {},
    };

    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        CommonModule,
        ProductsRoutingModule,
        RouterTestingModule.withRoutes(routes),
      ],
      declarations: [
        ProductsTableComponent,
        ProductsComponent,
        SearchComponent,
        ProductDetailsComponent,
        ProductCPEsTableComponent,
        ProductCVEsTableComponent,
      ],
      providers: [ProductsResolverService, ProductDetailsResolverService],
    })
      .overrideProvider(NzNotificationService, {
        useValue: notificationService,
      })
      .overrideProvider(ProductMatchingService, {
        useValue: productMatchingService,
      })
      .overrideProvider(AuthService, { useValue: authService })
      .overrideProvider(ActivatedRoute, { useValue: activatedRoute })
      .overrideProvider(Router, { useValue: router })
      .compileComponents()
      .then(() => {
        fixture = TestBed.createComponent(ProductDetailsComponent);
        component = fixture.componentInstance;
      });
  }));

  it('should create', () => {
    // Arrange & Act & Assert
    expect(component).toBeTruthy();
  });

  describe('ngOnDestroy()', () => {
    it('should unsubscribe loggedUserSubscription', () => {
      // Arrange
      const subscription = of(null).subscribe();
      const subSpy = jest.spyOn(subscription, 'unsubscribe');
      component['loggedUserSubscription'] = subscription;

      // Act
      component.ngOnDestroy();

      // Assert
      expect(subSpy).toHaveBeenCalledTimes(1);
    });
  });

  describe('sendEmail() should ', () => {
    it('call productsMatchingService getMatchingCPEs() once with correct parameter', () => {
      // Arrange
      const productId = 1;
      const mockProduct: Product = {
        id: 1,
        product: 'Compass',
        vendor: 'Act',
        version: '1.5.10',
        hasCpe: false,
        cpe: null,
      };
      const successSpy = jest.spyOn(notificationService, 'success');
      const spy = jest
        .spyOn(productMatchingService, 'sendEmail')
        .mockReturnValue(of(null));

      // Act
      component.sendEmail(mockProduct);

      // Assert
      expect(component.loadingSendEmail).toBeFalsy();
      expect(component.showNotifyAdminButton).toBeFalsy();
      expect(successSpy).toHaveBeenCalledWith(
        'Successfully sent an email to System Admin!',
        '',
        {
          nzPlacement: 'bottomRight',
        }
      );
      expect(spy).toBeCalledTimes(1);
    });
  });
});
