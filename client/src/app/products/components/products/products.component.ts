import { Component, OnInit } from '@angular/core';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { ActivatedRoute } from '@angular/router';
import { Product } from '../../models/product.model';
import { ProductsService } from '../../../../app/core/services/products.service';
import { SortBy } from '../../enums/sort-by.enum';
import { SortType } from '../../enums/sort-type.enum';
import {
  PaginationInfo,
  DataPaginationResponse,
  SortInfo,
  PageInfo,
} from '../../../../app/shared/models/data-pagination-response.model';
import { ProductsTableView } from '../../enums/products-table-view.enum';
import { ProductsCount } from '../../../../app/shared/models/products-count.model';
import {
  FormGroup,
  FormGroupDirective,
  FormBuilder,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss'],
})
export class ProductsComponent implements OnInit {
  public products: Product[] = [];
  public paginationInfo: PaginationInfo;
  public loadingPage: boolean;
  public productsTableView: ProductsTableView = ProductsTableView.all;
  public productsCount: ProductsCount;
  public isVisibleModal = false;
  public addProductForm: FormGroup;

  private searchString: string;
  private radioButtonOption: string;
  private pageInfo: PageInfo = { pageSize: 10, pageIndex: 1 };
  private sortInfo: SortInfo = { sortType: SortType.DESC, sortBy: SortBy.DATE };

  constructor(
    private readonly notificationService: NzNotificationService,
    private readonly activatedRoute: ActivatedRoute,
    private readonly productsService: ProductsService,
    private notification: NzNotificationService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(
      (data: { productsResolver: DataPaginationResponse<Product[]> }) => {
        this.products = data.productsResolver.data;
        this.paginationInfo = {
          page: data.productsResolver.page,
          resultsPerPage: data.productsResolver.resultsPerPage,
          totalResults: data.productsResolver.totalResults,
        };
      }
    );

    this.productsService
      .getProductsCount()
      .subscribe((count) => (this.productsCount = count));

    this.addProductForm = this.formBuilder.group({
      product: ['', [Validators.required]],
      vendor: ['', [Validators.required]],
      version: ['', [Validators.required]],
    });
  }

  public allProducts(): void {
    this.productsTableView = ProductsTableView.all;
    this.loadingPage = true;

    this.productsService
      .getAllProducts(
        this.pageInfo.pageIndex,
        this.pageInfo.pageSize,
        this.sortInfo.sortBy,
        this.sortInfo.sortType
      )
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.products = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public productsWithCVEs(): void {
    this.productsTableView = ProductsTableView.withCVEs;
    this.loadingPage = true;

    this.productsService
      .getProductsWithCVEs(
        this.pageInfo.pageIndex,
        this.pageInfo.pageSize,
        this.sortInfo.sortBy,
        this.sortInfo.sortType
      )
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.products = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public productsWithoutCVEs(): void {
    this.productsTableView = ProductsTableView.withoutCVEs;
    this.loadingPage = true;

    this.productsService
      .getProductsWithoutCVEs(
        this.pageInfo.pageIndex,
        this.pageInfo.pageSize,
        this.sortInfo.sortBy,
        this.sortInfo.sortType
      )
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.products = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public productsSort(sortInfo: SortInfo): void {
    this.sortInfo = sortInfo;
    if (this.productsTableView === ProductsTableView.all) {
      this.allProducts();
    } else if (this.productsTableView === ProductsTableView.withCVEs) {
      this.productsWithCVEs();
    } else if (this.productsTableView === ProductsTableView.withoutCVEs) {
      this.productsWithoutCVEs();
    } else {
      this.search();
    }
  }

  public productsPagination(pageInfo: PageInfo): void {
    this.pageInfo = pageInfo;
    if (this.productsTableView === ProductsTableView.all) {
      this.allProducts();
    } else if (this.productsTableView === ProductsTableView.withCVEs) {
      this.productsWithCVEs();
    } else {
      this.productsWithoutCVEs();
    }
  }

  public searchByVendor(): void {
    this.loadingPage = true;
    this.productsService
      .searchByVendor(
        this.searchString,
        this.pageInfo.pageIndex,
        this.pageInfo.pageSize,
        this.sortInfo.sortBy,
        this.sortInfo.sortType
      )
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.products = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public searchByProduct(): void {
    this.loadingPage = true;
    this.productsService
      .searchByProduct(
        this.searchString,
        this.pageInfo.pageIndex,
        this.pageInfo.pageSize,
        this.sortInfo.sortBy,
        this.sortInfo.sortType
      )
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.products = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public searchByCPE(): void {
    this.loadingPage = true;
    this.productsService
      .searchByCPE(
        this.searchString,
        this.pageInfo.pageIndex,
        this.pageInfo.pageSize,
        this.sortInfo.sortBy,
        this.sortInfo.sortType
      )
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.products = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public searchByCVE(): void {
    this.loadingPage = true;
    this.productsService
      .searchByCVE(
        this.searchString,
        this.pageInfo.pageIndex,
        this.pageInfo.pageSize,
        this.sortInfo.sortBy,
        this.sortInfo.sortType
      )
      .subscribe(
        (response) => {
          this.loadingPage = false;
          this.paginationInfo = {
            page: response.page,
            resultsPerPage: response.resultsPerPage,
            totalResults: response.totalResults,
          };
          this.products = response.data;
        },
        (err) => {
          this.loadingPage = false;
          this.notificationService.error(
            'Failed!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
  }

  public searchButtonClicked({
    searchString,
    radioButtonOption,
  }: {
    searchString: string;
    radioButtonOption: string;
  }): void {
    this.searchString = searchString;
    this.radioButtonOption = radioButtonOption;
    this.search();
  }

  public search(): void {
    switch (this.radioButtonOption) {
      case 'vendor':
        this.searchByVendor();
        break;
      case 'product':
        this.searchByProduct();
        break;
      case 'cpe':
        this.searchByCPE();
        break;
      case 'cveId':
        this.searchByCVE();
    }
  }

  public searchClicked(): void {
    this.searchString = '';
    this.productsTableView = ProductsTableView.search;
  }
  public addProduct(): void {
    this.isVisibleModal = true;
  }
  public handleCancel(): void {
    this.isVisibleModal = false;
  }

  public submitForm(form: FormGroupDirective): void {
    if (form.valid) {
      this.productsService.addProduct(form.value).subscribe(
        (res) => {
          this.products.unshift(res);
          this.notification.success('Successfully added product!', '', {
            nzPlacement: 'bottomRight',
          });
          this.handleCancel();
        },
        (err) => {
          this.notification.error(
            'Failed to add product!',
            err.error && err.error.message,
            { nzPlacement: 'bottomRight' }
          );
        }
      );
      form.resetForm();
    }
  }
}
