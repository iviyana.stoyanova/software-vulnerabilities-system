import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Product } from '../../models/product.model';
import { SortType } from '../../enums/sort-type.enum';
import { NzTableQueryParams } from 'ng-zorro-antd/table';
import { SortBy } from '../../enums/sort-by.enum';
import {
  SortInfo,
  PaginationInfo,
  PageInfo,
} from '../../../../app/shared/models/data-pagination-response.model';

@Component({
  selector: 'app-products-table',
  templateUrl: './products-table.component.html',
  styleUrls: ['./products-table.component.scss'],
})
export class ProductsTableComponent implements OnInit {
  public currentSortChange: { key: string; value: string } = {
    key: null,
    value: null,
  };

  @Input()
  public products: Product[];
  @Input()
  public pagination: PaginationInfo;
  @Input()
  public loading: boolean;

  @Output()
  public onPageClicked: EventEmitter<PageInfo> = new EventEmitter();
  @Output()
  public onSortClicked: EventEmitter<SortInfo> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  public onPageNumberChange(page: number): void {
    const params: PageInfo = {
      pageSize: this.pagination.resultsPerPage,
      pageIndex: page,
    };
    this.onPageClicked.emit(params);
  }

  public onPageSizeChange(size: number): void {
    const params: PageInfo = {
      pageSize: size,
      pageIndex: this.pagination.page,
    };
    this.onPageClicked.emit(params);
  }

  public onQueryParamsChange(params: NzTableQueryParams): void {
    const { sort } = params;
    const sortChange = sort.find((el) => el.value);
    if (
      sortChange &&
      (sortChange.value !== this.currentSortChange.value ||
        sortChange.key !== this.currentSortChange.key)
    ) {
      this.currentSortChange = sortChange;
      const sortInfo: SortInfo = {
        sortType: sortChange.value === 'ascend' ? SortType.ASC : SortType.DESC,
        sortBy: sortChange.key as SortBy,
      };
      this.onSortClicked.emit(sortInfo);
    }
  }
}
