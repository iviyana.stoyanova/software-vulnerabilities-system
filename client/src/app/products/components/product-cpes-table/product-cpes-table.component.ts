import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CPE } from '../../models/cpe.model';

@Component({
  selector: 'app-product-cpes-table',
  templateUrl: './product-cpes-table.component.html',
  styleUrls: ['./product-cpes-table.component.scss'],
})
export class ProductCPEsTableComponent implements OnInit {
  @Input()
  public productCPEs: CPE[];

  @Input()
  public loading: boolean;

  @Output()
  public assignCPE: EventEmitter<CPE> = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  public onAssignClicked(cpe: CPE): void {
    this.assignCPE.emit(cpe);
  }

  public sortDate = (a: CPE, b: CPE) =>
    a.lastModifiedDate.localeCompare(b.lastModifiedDate)
}
