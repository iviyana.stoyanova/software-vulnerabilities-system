export enum ProductsTableView {
  all = 'all',
  withCVEs = 'withCVEs',
  withoutCVEs = 'withoutCVEs',
  search = 'search'
}
