import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { ProductsComponent } from './components/products/products.component';
import { ProductsResolverService } from './resolvers/products.resolver';
import { AuthGuard } from '../core/guards/auth.guard';
import { ProductDetailsResolverService } from './resolvers/product-details.resolver';

const routes: Routes = [
  {
    path: '',
    component: ProductsComponent,
    resolve: { productsResolver: ProductsResolverService },
    canActivate: [AuthGuard],
  },
  {
    path: ':productId',
    component: ProductDetailsComponent,
    resolve: { productDetailsResolver: ProductDetailsResolverService },
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsRoutingModule {}
