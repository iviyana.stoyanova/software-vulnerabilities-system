import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, timeout, tap } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Product } from '../models/product.model';
import { ProductsService } from '../../../app/core/services/products.service';
import { SortBy } from '../enums/sort-by.enum';
import { SortType } from '../enums/sort-type.enum';
import { DataPaginationResponse } from '../../../app/shared/models/data-pagination-response.model';

@Injectable()
export class ProductsResolverService
  implements Resolve<DataPaginationResponse<Product[]>> {
  constructor(
    private readonly productsService: ProductsService,
    private readonly notificationService: NzNotificationService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(
    route: ActivatedRouteSnapshot
  ): Observable<DataPaginationResponse<Product[]>> {
    this.spinner.show();
    return this.productsService
      .getAllProducts(1, 10, SortBy.DATE, SortType.DESC)
      .pipe(
        tap(() => {
          this.spinner.hide();
        }),
        timeout(5000),
        catchError((err) => {
          this.spinner.hide();
          this.notificationService.error(
            'Failed! The NVD Data Feeds is not accessible!',
            err.error && err.error.message,
            { nzPlacement: 'topRight' }
          );
          const emptyData: DataPaginationResponse<Product[]> = {
            resultsPerPage: null,
            page: null,
            totalResults: null,
            data: [],
          };

          return of(emptyData);
        })
      );
  }
}
