import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { catchError, timeout, tap } from 'rxjs/operators';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { of } from 'rxjs';
import { NgxSpinnerService } from 'ngx-spinner';
import { Product } from '../models/product.model';
import { ProductsService } from '../../../app/core/services/products.service';

@Injectable()
export class ProductDetailsResolverService implements Resolve<Product> {
  constructor(
    private readonly productsService: ProductsService,
    private readonly notificationService: NzNotificationService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Product> {
    const id = route.paramMap.get('productId');
    this.spinner.show();
    return this.productsService.getProduct(+id).pipe(
      tap(() => {
        this.spinner.hide();
      }),
      timeout(1000),
      catchError((err) => {
        this.spinner.hide();
        this.notificationService.error(
          'Failed! The Database is not accessible!',
          err.error && err.error.message,
          { nzPlacement: 'topRight' }
        );
        const emptyData: Product = {
          id: null,
          product: null,
          vendor: null,
          version: null,
          addedOn: null,
          hasCves: null,
          hasCpe: null,
          cpe: null,
          username: null,
        };

        return of(emptyData);
      })
    );
  }
}
