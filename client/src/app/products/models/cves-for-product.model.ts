import { CVE } from '../../../app/home/models/cve.model';

export interface CVEsForProduct {
    assignedCVEs: CVE[];
    unassignedCVEs: CVE[];
}
