export interface CPE {
  title: string;
  cpeUri: string;
  lastModifiedDate: string;
  deprecatedBy: string;
}
