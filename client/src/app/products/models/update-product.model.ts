export class UpdateProduct {
    public product?: string;
    public vendor?: string;
    public version?: string;
}
