import { CPE } from './cpe.model';

export interface Product {
  id?: number;
  product: string;
  vendor: string;
  version: string;
  addedOn?: string;
  hasCves?: boolean;
  hasCpe?: boolean;
  cpe?: CPE;
  username?: string;
}
