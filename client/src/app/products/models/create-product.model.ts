export class CreateProduct {
  product: string;
  vendor: string;
  version: string;
}
