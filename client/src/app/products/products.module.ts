import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsTableComponent } from './components/products-table/products-table.component';
import { ProductsComponent } from './components/products/products.component';
import { ProductsRoutingModule } from './products-routing.module';
import { SharedModule } from '../shared/shared.module';
import { ProductsResolverService } from './resolvers/products.resolver';
import { SearchComponent } from './components/search/search.component';
import { ProductDetailsComponent } from './components/product-details/product-details.component';
import { ProductCPEsTableComponent } from './components/product-cpes-table/product-cpes-table.component';
import { ProductCVEsTableComponent } from './components/product-cves-table/product-cves-table.component';
import { ProductDetailsResolverService } from './resolvers/product-details.resolver';

@NgModule({
  declarations: [
    ProductsTableComponent,
    ProductsComponent,
    SearchComponent,
    ProductDetailsComponent,
    ProductCPEsTableComponent,
    ProductCVEsTableComponent,
  ],
  imports: [CommonModule, SharedModule, ProductsRoutingModule],
  providers: [ProductsResolverService, ProductDetailsResolverService],
})
export class ProductsModule {}
