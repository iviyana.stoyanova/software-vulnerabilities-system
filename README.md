# <img src="client/src/assets/svs-logo.png" alt="svs-logo" border="0" width="80px"> SVS - Software vulnerabilities system

## Description

Final Project Assignment for **Telerik Academy Alpha with JavaScript**: Design and implement a system to automate the process of finding possible vulnerabilities in software products installed inside the organization. The system receives as input a list of software products (the inventory) in JSON format. Each JSON document contains three attributes: vendor, product, and software version.

</br>

## In order to run the project
  1. Clone the project: 

      ```git clone https://gitlab.com/iviyana.stoyanova/software-vulnerabilities-system.git```

  2. Setup a schema in MySQL Database. Make sure that the charset is **utf8mb4** and collation is **utf8mb4_unicode_ci**.

  3. Setup **.env** file in the server folder of the project.
      ```
      PORT=3000
      DB_TYPE=mysql
      DB_HOST=localhost
      DB_PORT=3306
      DB_USERNAME=root
      DB_PASSWORD=1234
      DB_DATABASE_NAME=svsdb
      JWT_SECRET=abracadabra
      ```
  4. run `npm install` both in server and client folders to install all dependencies

## Scripts
- Server side (in server folder)
  - `npm run start` - Start application
  - `npm run start:dev` - Start application in dev mode
  - `npm run seed` - Initial seed for the database
  - `npm run test` - Run Jest test runner
- Client side (in client folder)
  - `ng s` or `ng s -o` - Starts the application in your browser
  - `ng test` - Run Jest test runner

## Entity-relationship in Database
<img src="https://i.ibb.co/mFVtS3g/eer-diagram.png" alt="eer-diagram" width ="450px">

## Project Overview
- Home page
  <img src="https://i.ibb.co/hLwrTmb/home-page.png" alt="home-page">
- Products dashboard
  <img src="https://i.ibb.co/QCD0Myd/products-dashboard-page.png" alt="products-dashboard">
- Products search
  <img src="https://i.ibb.co/M5v3Sfm/products-search.png" alt="products-search">
- Product details and assign CPE
  <img src="https://i.ibb.co/pz4S4jq/assign-cpe.png" alt="product-details-and-cpe-assignment">
- Product details and assign CVEs
  <img src="https://i.ibb.co/93FcrtW/assign-cve.png" alt="product-details-and-cve-assignment">
- Admin panel
  <img src="https://i.ibb.co/Gty3GkK/image.png" alt="admin-panel">
- User details
  <img src="https://i.ibb.co/r4k8d8p/user-details.png" alt="user-details">
- Not-found page
  <img src="https://i.ibb.co/H7RVzW0/not-found-page.png" alt="not-found">

## Postman Collection
A Postman collection can be found <a href="https://gitlab.com/iviyana.stoyanova/software-vulnerabilities-system/-/tree/master/server/src/postman">here</a>

## Authors
Team 4:  <a href="https://gitlab.com/iviyana.stoyanova">Iviyana Stoyanova</a> and <a href="https://gitlab.com/beckybellnay">Izabela Naydenova</a>